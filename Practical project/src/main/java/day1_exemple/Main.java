package day1_exemple;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<String> listObj1 = Arrays.asList("a","b","c", "d", "cristi" );
        List<String> listObj2 = listObj1.stream().filter(var -> var != "c").collect(Collectors.toList());
        Optional<String> listObj3 = listObj1.stream().filter(c -> c.equals("cristi")).findFirst();

        System.out.println("Lista 1: " + listObj1);
        System.out.println("Lista filtrata(fara c):" + listObj2);
        System.out.println("Lista filtrata(primul c: )"+ listObj3);
    }
}
