package com.sda.practical.databases;

import javax.persistence.*;

@Entity
@Table(name = "review")
public class ReviewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reviewId;
    private String title;
    private String comment;
   // @Column(insertable = false, updatable = false)
    private Integer bookId;

    @ManyToOne
    @JoinColumn(name = "bookId", insertable = false, updatable = false)
    private BookEntity book;

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public BookEntity getBook() {
        return book;
    }

    public void setBook(BookEntity book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "ReviewEntity{" +
                "reviewId=" + reviewId +
                ", title='" + title + '\'' +
                ", comment='" + comment + '\'' +
                ", bookId=" + bookId +
                ", book=" + book +
                '}';
    }
}

