package com.sda.practical.services;

import com.sda.practical.databases.ReviewEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class ReviewServices extends BaseServices {
    public ReviewServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewReview(){

        List<ReviewEntity> reviewEntities = getAllReviews();
        System.out.println();
        System.out.printf("%-25S | %-20S |%-15S |%s %n", "Denumire carte", "Autor", "Titlu Review","Review" );
        System.out.println("----------------------------------------------------------------------------------------------------------------------------");
        reviewEntities.stream().forEach(reviewEntity->{
            System.out.printf("%-25S | %-20s |%-15S |%s %n", reviewEntity.getBook().getName(), reviewEntity.getBook().getAuthor(), reviewEntity.getTitle(), reviewEntity.getComment());
        });
        System.out.println();
        System.out.println("---------------------");
    }

    public void addReview(Scanner scanner){
        //0. afiseaza lista de carti:
        viewReview();

        //1. creaza o entitate goala;
        ReviewEntity reviewEntity = new ReviewEntity();

        //2. cere utilizatorului sa selecteze cartea pentru care doreste sa adauge un review;

        System.out.println("Introduceti id-ul cartii pentru care doriti sa adaugati review: ");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();

        //3. cere utilizatorului sa adauge campurile pentru un nou review

        System.out.println("Introduceti un titlu de review: ");
        String titlu = scanner.nextLine();

        System.out.println("Introduceti comment-ul: ");
        String comment = scanner.nextLine();

        //3. salvare inregistrare;

        reviewEntity.setBookId(bookId);
        reviewEntity.setTitle(titlu);
        reviewEntity.setComment(comment);
        saveReview(reviewEntity);
    }

    private List<ReviewEntity> getAllReviews(){
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from ReviewEntity", ReviewEntity.class).list();
    }
    private void saveReview(ReviewEntity reviewEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(reviewEntity);
        transaction.commit();
        session.close();
    }
}
