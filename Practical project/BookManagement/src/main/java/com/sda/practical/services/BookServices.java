package com.sda.practical.services;

import com.sda.practical.databases.BookEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class BookServices extends BaseServices {
    public BookServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewBook() {
        List<BookEntity> books = getAllBooks();
        System.out.printf("%-3S |%-30S |%-30S |%-25S |%-20S |%S %n", "ID", "ISBN", "Denumire Carte","Autor", "Editura","Anul");
        System.out.println("---------------------------------------------------------------------------------------------");
        books.stream().forEach(b -> {
            System.out.printf("%-3d |%-30s |%-30s |%-25s |%-20s |%d %n", b.getBookId(),b.getIsbn(),b.getName(), b.getAuthor(), b.getPublishingHouse(), b.getYear());
        });
        System.out.println();
    }


    public void addBook(Scanner scanner) {

        //1. creaza o entitate goala;
        BookEntity bookEntity = new BookEntity();

        //2. cere utilizatorului sa introduca datele cartii
        System.out.println("Introduceti o denumire de carte: ");
        String name = scanner.nextLine();

        System.out.println("Introduceti un cod ISBN nou: ");
        String isbn = scanner.nextLine();

        System.out.println("Introduceti editura cartii: ");
        String editura = scanner.nextLine();

        System.out.println("Introduceti anul cartii: ");
        Integer an = scanner.nextInt();

        System.out.println("Introduceti authorId: ");
        Integer authorId = scanner.nextInt();
        // AuthorEntity authorEntity = getAuthor(authorId);

        //3. salveaza modificarile
        bookEntity.setName(name);
        bookEntity.setIsbn(isbn);
        bookEntity.setPublishingHouse(editura);
        bookEntity.setYear(an);
        // bookEntity.setAuthor(authorEntity);
        bookEntity.setAuthorId(authorId);
        saveBook(bookEntity);

    }

    public void editBook(Scanner scanner) {
        //1. vizualizeaza lista de carti
        viewBook();

        //2. cere utilizatorului sa introduca  bookId pentru cartea dorita
        System.out.println("Introduceti bookId pentru care doriti sa se faca editarea: ");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();

        //3. editeaza campurile cartii
        BookEntity bookEntity = getBook(bookId);
        System.out.println("Introduceti numele cartii: ");
        String name = scanner.nextLine();

        System.out.println("Introduceti codul ISBN: ");
        String isbn = scanner.nextLine();

        System.out.println("Introduceti editura cartii: ");
        String editura = scanner.nextLine();

        System.out.println("Introduceti anul cartii: ");
        Integer an = scanner.nextInt();


        //3. salveaza modificarile
        bookEntity.setName(name);
        bookEntity.setIsbn(isbn);
        bookEntity.setPublishingHouse(editura);
        bookEntity.setYear(an);
        updateBook(bookEntity);

    }

    public void deleteBook(Scanner scanner) {
        //1. Vizualizeaza lsita de carti cu toate campurile;
        viewBook();
        System.out.println("Selecteaza alta optiune: ");

        //2. Cere utilizatorului sa introduca un bookId pentru selectia unei carti;
        System.out.println("Intrduceti bookId pentru selectia unei carti: ");
        Integer bookId = scanner.nextInt();
        scanner.nextLine();

        //3. Sterge inregistrarea cu bookId selectat;
        BookEntity bookEntity = getBook(bookId);
        delete(bookEntity);
    }


    private void saveBook(BookEntity bookEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(bookEntity);
        transaction.commit();
        session.close();
    }

    private void delete(BookEntity bookEntity) {
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(bookEntity);
        transaction.commit();
        session.close();
    }

    private void updateBook(BookEntity bookEntity){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(bookEntity);
        transaction.commit();
        session.close();
    }


}