package com.sda.practical.databases;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "author")
public class AuthorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer authorId;

    private String name;

    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)//populeaza lista de carti de la inceput;
    // default este lazzy-se populeaza doar cand apelam metoda getBooks();

    private List<BookEntity> books;


    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BookEntity> getBooks() {
        return books;
    }

    public void setBooks(List<BookEntity> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return  authorId + " "+ name;
    }
}
