package com.sda.practical.services;

import com.sda.practical.databases.AuthorEntity;
import com.sda.practical.databases.BookEntity;
import org.hibernate.Session;

import java.util.List;

public abstract class BaseServices {
private HibernateService hibernateService;

    public BaseServices(HibernateService hibernateService) {
        this.hibernateService = hibernateService;
    }

    public HibernateService getHibernateService() {
        return hibernateService;
    }

    protected AuthorEntity getAuthor(Integer authorId){
        Session session = getHibernateService().getSessionFactory().openSession();
        AuthorEntity authorEntity = session.find(AuthorEntity.class, authorId);
        session.close();
        return authorEntity;
    }
    protected BookEntity getBook(Integer bookId) {
        Session session = getHibernateService().getSessionFactory().openSession();
        BookEntity bookEntity = session.find(BookEntity.class, bookId);
        session.close();
        return bookEntity;
    }
    protected List<BookEntity> getAllBooks() {
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from BookEntity", BookEntity.class).list();
    }

}
