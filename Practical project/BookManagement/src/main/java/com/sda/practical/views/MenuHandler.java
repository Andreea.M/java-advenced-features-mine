package com.sda.practical.views;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuHandler {
    private Map<MenuTypeEnum, List<String>> menus;

    public MenuHandler() {
        menus = new HashMap<>();
        List<String> mainMenu = new ArrayList<>();
        mainMenu.add("1. Management Autor");
        mainMenu.add("2. Management Carti");
        mainMenu.add("3. Management Review");
        mainMenu.add("9. Exit");
        menus.put(MenuTypeEnum.MAIN_MENU, mainMenu);

        List<String> authorMenu = new ArrayList<>();
        authorMenu.add("1 Vizualizeaza autor");
        authorMenu.add("2 Adauga autor");
        authorMenu.add("3 Actualizeaza lista de autori");
        authorMenu.add("4 Sterge autor");
        authorMenu.add("9 Meniu principal");
        menus.put(MenuTypeEnum.AUTHOR_MENU, authorMenu);

        List<String> bookMenu = new ArrayList<>();
        bookMenu.add("1. Vizualizeaza lista de carti");
        bookMenu.add("2. Adauga o carte");
        bookMenu.add("3. Actualizeaza lista de carti");
        bookMenu.add("4. Sterge o carte");
        bookMenu.add("9. Meniu principal");
        menus.put(MenuTypeEnum.BOOK_MENU, bookMenu);

        List<String> reviewMenu = new ArrayList<>();
        reviewMenu.add("1. Vizualizeaza review-uri");
        reviewMenu.add("2. Adauga un review");
        reviewMenu.add("9. Meniu principal");
        menus.put(MenuTypeEnum.REVIEW_MENU, reviewMenu);

    }

    public void printMenu(MenuTypeEnum menuType) {
//        List<String> menuList = menus.get(menuType);
//        for (String menuValue : menuList) {
//            System.out.println(menuValue);
//        }
        menus.getOrDefault(menuType, new ArrayList<>()).stream().forEach(System.out::println);
    }

}
