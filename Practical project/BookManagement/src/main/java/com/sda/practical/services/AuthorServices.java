package com.sda.practical.services;

import com.sda.practical.databases.AuthorEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class AuthorServices extends BaseServices{
    public AuthorServices(HibernateService hibernateService) {
        super(hibernateService);
        System.out.println(getClass().getSimpleName() + " created");
    }

    public void viewAuthor(){
        System.out.println();
        List<AuthorEntity> authors = getAllAuthors();
        System.out.printf("%-3S|%-20S %n","ID", "Autor");
        System.out.println("--------------------------------------------");
        authors.stream().forEach(authorEntity-> {
            System.out.printf( "%-3d|%-20S %n", authorEntity.getAuthorId(), authorEntity.getName());
        });

        //2. cere utilizatorului sa selecteze un autor;
        System.out.println("----------------");
        System.out.println("Selecteaza alta optiune: ");
    }

    public void addAuthor(Scanner scanner){
        //1. creaza o entitate goala;
        AuthorEntity authorEntity = new AuthorEntity();

        //2. cere utilizatorului sa introduca campurile inregistrarii;
        System.out.println("Introduceti numele autorului: ");
        String name = scanner.nextLine();

        //3. salvare inregistrare;
        authorEntity.setName(name);
        saveAuthor(authorEntity);
    }

    public void editAuthor(Scanner scanner){
        //1. afiseaza lista de autori;
        viewAuthor();

        //2. cere utilizatorului sa selecteze un autor;
        System.out.println("Selectati un autor: ");

        Integer authorId = scanner.nextInt();
        scanner.nextLine();

        //3. editeaza autorul;
        AuthorEntity authorEntity = getAuthor(authorId);
        System.out.println("Introduceti numele autorului: ");
        String name = scanner.nextLine();

        //4. salveaza autorul;
        authorEntity.setName(name);
        updateAuthor(authorEntity);
    }


    public void deleteAuthor(Scanner scanner){
        //1. afisare lista de autori
        viewAuthor();

        //2. cere utilizatorului sa introduca un Id pentru selectie autor
        System.out.println("Introduceti un authorId pentru selectie autor: ");
        Integer authorId = scanner.nextInt();
        scanner.nextLine();

        //3. stergere autor din lista
        AuthorEntity authorEntity = getAuthor(authorId);
        deleteAuthor(authorEntity);

    }

    private List<AuthorEntity> getAllAuthors(){
        Session session = getHibernateService().getSessionFactory().openSession();
        return session.createQuery("from AuthorEntity", AuthorEntity.class).list();
    }



    private void updateAuthor(AuthorEntity authorEntity){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(authorEntity);
        transaction.commit();
        session.close();
    }


    private void deleteAuthor(AuthorEntity authorEntity){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(authorEntity);
        transaction.commit();
        session.close();
    }
    private void saveAuthor(AuthorEntity authorEntity){
        Session session = getHibernateService().getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(authorEntity);
        transaction.commit();
        session.close();
    }


}
