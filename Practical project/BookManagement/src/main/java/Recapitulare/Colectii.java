package Recapitulare;

import java.util.*;

public class Colectii {
    public static void main(String[] args) {
        ArrayList<String> listaMeaDeStringuri = new ArrayList<>();
        List<String> listaMeaDeStringuri1 = new ArrayList<>();
        List<String> listaMea = new LinkedList<>();
        Set<String> mySet = new HashSet<>();


        ArrayList<Integer> listaMeaDeNumere = new ArrayList<>();

        System.out.println("Marime lista: "+listaMeaDeStringuri.size());
        listaMeaDeStringuri.add("Cristian");
        listaMeaDeStringuri.add("Gabriel");
        listaMeaDeStringuri.add("Dan");
        listaMeaDeStringuri.add("Florin");

        System.out.println("marime lista dupa adaugare nume: " + listaMeaDeStringuri.size());
        for(String elementDinLista : listaMeaDeStringuri){
            System.out.println(elementDinLista);
        }

        listaMeaDeStringuri.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length() == o2.length()) {
                    return 0;
                }
                return o1.length()>o2.length() ? -1 : 1;
            }
        }.reversed());

        Iterator it = listaMeaDeStringuri.iterator();
        while(it.hasNext()) {
            String val = (String)it.next();
            if(val.equals("Gabriel")) {
                it.remove();
                System.out.println("am sters");
                break;
            }
        }

        System.out.println("----------------------------");
        Set<String> mySet1 = new HashSet<>();
        mySet.add("cristi");
        mySet.add("florin");
        mySet.add("cristi");
        for(String val : mySet) {
            System.out.println(val);
        }

        System.out.println("----------------------------");

        Set<Users> users = new HashSet<>();
        users.add(new Users(1, "Cristi", "Cluj"));
        users.add(new Users(2, "Dan", "Timisoara"));
        users.add(new Users(3, "Cristi","Iasi"));
        users.add(new Users(2, "Cristi","Bacau"));
        users.forEach(System.out::println);
        System.out.println(users.size());
    }
}
