package Recapitulare;

import java.io.Serializable;

//implements Serializable este foarte important daca vrem sa serializam obiectul
public class Users implements Serializable {
    private Integer id;
    private String name;
    private String adress;


    public Users() {
    }

    public Users(Integer id, String name, String adress) {
        this.id = id;
        this.name = name;
        this.adress = adress;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User " + id + " has name the: " + name + ", and the adress: " + adress;
    }
}
