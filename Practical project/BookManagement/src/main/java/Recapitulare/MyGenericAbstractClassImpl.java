package Recapitulare;

/**
 * Class
 * Developer: Cristian Ando-Ciupav
 * Email: accexpert@gmail.com
 * Date: 11/21/2020
 * Project: javaadvanced
 */
public class MyGenericAbstractClassImpl extends MyGenericAbstractClass<Double> {

    @Override
    public void myAbstractMethod(Double myParameter) {

    }
}
