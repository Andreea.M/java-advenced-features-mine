package Recapitulare;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashMapEx {
    public static void main(String[] args) {
        Map<Integer, String> myMap = new HashMap<>();
        Map<Integer, Integer> myMap1 = new HashMap<>();
        myMap.put(1, "cristi");
        myMap.put(2, "dan");
        Map<String, Users> myUsers = new HashMap<>();

        Map<String, List<Users>> myMap2 = new HashMap<>();
        Map<String, Map<String, Users>> myMap3 = new HashMap<>();

        for(Map.Entry<Integer, String> val : myMap.entrySet()){
            System.out.println("Key: " + val.getKey() + " - Value: " + val.getValue());
        }

        String value = myMap.get(1);

        String valueOrDefault = myMap.getOrDefault(1, "nothing");
        System.out.println(valueOrDefault);

        boolean contains = myMap.containsKey(1);
        System.out.println("Contains: "+contains);

        myMap.replace(1, "cristi", "cristian");
        System.out.println(myMap.get(1));

        System.out.println("Map size: "+myMap.size());

        myMap.remove(1);
        for(Map.Entry<Integer, String> val : myMap.entrySet()) {
            System.out.println("Key: "+val.getKey()+" - Value: "+val.getValue());
        }
    }
}
