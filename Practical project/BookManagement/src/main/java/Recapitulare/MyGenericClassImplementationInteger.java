package Recapitulare;

/**
 * Class
 * Developer: Cristian Ando-Ciupav
 * Email: accexpert@gmail.com
 * Date: 11/21/2020
 * Project: javaadvanced
 */
public class MyGenericClassImplementationInteger implements MyGenericInterface<Integer, String> {

    @Override
    public void myGenericMethod(Integer myParameter) {

    }

    @Override
    public String myMethod(Integer myParameter) {
        return null;
    }
}
