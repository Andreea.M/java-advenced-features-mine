package Recapitulare;

/**
 * Class
 * Developer: Cristian Ando-Ciupav
 * Email: accexpert@gmail.com
 * Date: 11/21/2020
 * Project: javaadvanced
 */
public class MyGenericClassImplementation implements MyGenericInterface<String, Integer> {

    @Override
    public void myGenericMethod(String myParameter) {

    }

    @Override
    public Integer myMethod(String myParameter) {
        return null;
    }
}
