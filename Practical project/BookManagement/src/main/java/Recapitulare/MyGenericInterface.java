package Recapitulare;

/**
 * Class
 * Developer: Cristian Ando-Ciupav
 * Email: accexpert@gmail.com
 * Date: 11/21/2020
 * Project: javaadvanced
 */
public interface MyGenericInterface<T, E> {
    void myGenericMethod(T myParameter);

    E myMethod(T myParameter);
}
