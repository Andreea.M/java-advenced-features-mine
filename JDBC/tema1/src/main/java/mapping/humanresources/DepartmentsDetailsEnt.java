package mapping.humanresources;

public class DepartmentsDetailsEnt {
    private int departmentId;
    private String departmentName;

    public DepartmentsDetailsEnt(){

    }

    public DepartmentsDetailsEnt(int departmentId, String departmentName){
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }
    public DepartmentsDetailsEnt(int departmentId){
        this.departmentId = departmentId;
    }

    public int getDepartmentId(){
        return departmentId;
    }

    public void setDepartmentId(int departmentId){
        this.departmentId = departmentId;
    }

    public String getDepartmentName(){
        return departmentName;
    }

    public void setDepartmentName(String departmentName){
        this.departmentName = departmentName;
    }

    @Override
    public String toString() {
        return "DepartmentsDetailsEnt{" +
                "departmentId=" + departmentId +
                ", departmentName='" + departmentName + '\'' +
                '}';
    }
}
