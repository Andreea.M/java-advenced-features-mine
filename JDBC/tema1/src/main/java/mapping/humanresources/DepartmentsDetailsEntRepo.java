package mapping.humanresources;

import Tema1.Utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentsDetailsEntRepo {

    public void findAll(){
        try(
                Connection conn = DriverManager.getConnection(
                        Utils.DATABASE_HOST,
                        Utils.DATABASE_USERNAME,
                        Utils.DATABASE_PASSWORD);
                PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Departments")){
                ResultSet rs = null;
                rs = preparedStatement.executeQuery();
            List<DepartmentsDetailsEnt> orders = new ArrayList<DepartmentsDetailsEnt>();
            while(rs.next()){
                orders.add(new DepartmentsDetailsEnt(rs.getInt(1),rs.getString(2)));
            }
            System.out.println(orders.toString());
        }catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }

    public void findByld(int nrDep){
        try(
        Connection conn = DriverManager.getConnection(
                Utils.DATABASE_HOST,
                Utils.DATABASE_USERNAME,
                Utils.DATABASE_PASSWORD);
        PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Departments where departmentId = ?")){
            preparedStatement.setInt(1,nrDep);
            ResultSet rs = null;
            rs = preparedStatement.executeQuery();
            List<DepartmentsDetailsEnt> orders = new ArrayList<DepartmentsDetailsEnt>();
            while(rs.next()){
                orders.add(new DepartmentsDetailsEnt(rs.getInt(1), rs.getString(2)));
            }
            System.out.println(orders.toString());
        }catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }
    }

//    public void save(){
//        try(
//                Connection conn = DriverManager.getConnection(
//                        Utils.DATABASE_HOST,
//                        Utils.DATABASE_USERNAME,
//                        Utils.DATABASE_PASSWORD);
//                PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Departments")){
//            ResultSet rs = null;
//            rs = preparedStatement.executeQuery();
//            List<DepartmentsDetailsEnt> orders = new ArrayList<DepartmentsDetailsEnt>();
//            while(rs.next()){
//                orders.add(new DepartmentsDetailsEnt(rs.getInt(1),rs.getString(2)));
//            }
//            System.out.println(orders.toString());
//        }catch (SQLException throwables)
//        {
//            throwables.printStackTrace();
//        }
//    }
    public boolean delete(int row){
        try(
                Connection conn = DriverManager.getConnection(
                        Utils.DATABASE_HOST,
                        Utils.DATABASE_USERNAME,
                        Utils.DATABASE_PASSWORD);
                PreparedStatement prepStat = conn.prepareStatement("DELETE FROM departments WHERE departmentId=?")) {
            prepStat.setInt(1,row);
            int size = prepStat.executeUpdate();
            if (size > 0)
                return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
