package Tema1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) {
        Connection conn;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("conectare cu succes");
            System.out.println("------------------------------------------");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM departments");
            while (rs.next()) {
                Integer deptId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                System.out.println(deptId + " " + deptName);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Connection conn1;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn1 = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("Display all projects (projectId, description)");
            System.out.println("------------------------------------------");
            Statement stmt = conn1.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM projects");

            while (rs.next()) {
                Integer prId = rs.getInt("projectId");
                String prDescr = rs.getString("description");
                System.out.println(prId + " " + prDescr);
            }
            rs.close();
            stmt.close();
            conn1.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Connection conn2;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn2 = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("Display all employees (employeeId, firstName, lastName, dateOfBirth): ");
            System.out.println("------------------------------------------");
            Statement stmt = conn2.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employees");
            while (rs.next()) {
                Integer employeeId = rs.getInt("employeeId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dateOfBirth = rs.getString("dateOfBirth");
                System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth);
            }
            rs.close();
            stmt.close();
            conn2.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        Connection conn3;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn3 = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("Display all employees with names starting with the letter J (employeeId, firstName, lastName,dateOfBirth): ");
            System.out.println("------------------------------------------");
            Statement stmt = conn3.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employees where firstName or lastName like 'J%'");
            while (rs.next()) {
                Integer employeeId = rs.getInt("employeeId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dateOfBirth = rs.getString("dateOfBirth");
                System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth);
            }
            rs.close();
            stmt.close();
            conn3.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        Connection conn4;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn4 = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("Display all employees that haven’t been assigned to a department: ");
            System.out.println("------------------------------------------");
            Statement stmt = conn4.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employees where departmentId is NULL");
            while (rs.next()) {
                Integer employeeId = rs.getInt("employeeId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dateOfBirth = rs.getString("dateOfBirth");
                System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth);
            }
            rs.close();
            stmt.close();
            conn4.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        Connection conn5;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn5 = DriverManager.getConnection(
                    Utils.DATABASE_HOST,
                    Utils.DATABASE_USERNAME,
                    Utils.DATABASE_PASSWORD);
            System.out.println("Display all employees along with the department they’re in (employeeId, firstName, lastName, dateOfBirth, departmentName) ");
            System.out.println("------------------------------------------");
            Statement stmt = conn5.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employees, depataments where employees.departmentId=depataments.departmentId");
            while (rs.next()) {
                Integer employeeId = rs.getInt("employeeId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dateOfBirth = rs.getString("dateOfBirth");
                String departmentName = rs.getString("departmentName");
                System.out.println(employeeId + " " + firstName + " " + lastName + " " + dateOfBirth + " " + departmentName);
            }
            rs.close();
            stmt.close();
            conn5.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}