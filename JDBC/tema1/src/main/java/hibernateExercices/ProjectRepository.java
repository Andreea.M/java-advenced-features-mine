package hibernateExercices;

import hibernate.Util;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ProjectRepository {

    public void findByld(int projectId) {
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            ProjectEnt project = session.find(ProjectEnt.class, projectId);
            System.out.println(project.toString());
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
        public void save(ProjectEnt project){
            Transaction transaction = null;
            try {
                Session session = Util.getSessionFactory().openSession();
                // start a transaction
                transaction = session.beginTransaction();
                // save the person object
                session.save(project);
                // commit transaction
                transaction.commit();
            } catch (Exception ex) {
                if (transaction != null) {
                    transaction.rollback();
                }
                ex.printStackTrace();
            }
        }

    public void delete(ProjectEnt project, int projectId){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            project.setProjectId(projectId);
            session.delete(project);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void update(ProjectEnt project, int projectId){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            project.setProjectId(projectId);
            session.update(project);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
