package hibernateSlide62;

import javax.persistence.*;
@Entity
@Table(name="projects")
public class ProjectEnt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projectId")
    private int proejctId;

    @Column(name = "description")
    private String projectName;


    public ProjectEnt(){};

    public ProjectEnt(int proejctid, String projectName) {
        this.proejctId = proejctid;
        this.projectName = projectName;

    }

    public int getProejctid() {
        return proejctId;
    }

    public void setProejctId(int proejctid) {
        this.proejctId = proejctid;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "ProjectEnt{" +
                "proejctId=" + proejctId +
                ", projectName='" + projectName + '\'' +
                '}';
    }
}
