package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
public class AlienDAO {
    public void createAlien(AlienEnt alien){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void updateAlien(AlienEnt alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.update(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteAlien(AlienEnt alien, int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            alien.setAlienId(id);
            session.delete(alien);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void findAlien(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            AlienEnt alien = session.find(AlienEnt.class, id);
            System.out.println(alien.toString());
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
    public void deleteById(int id){
        Transaction transaction = null;
        try {
            Session session = Util.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            AlienEnt alien = null;
            alien = session.load(AlienEnt.class, id);
            session.delete(alien);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }
}
