package crud;

import java.sql.*;

public class CrudUtils {
    public void selectProduct(double a, double b) {
        //partea de logare
        Connection con = null;
        try {
            //ne conectam la bd
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=SDA131208&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection succesful");
            ResultSet rs = null;
            PreparedStatement stmt = con.prepareStatement("SELECT * FROM products WHERE buyPrice BETWEEN ? AND ?");
            stmt.setDouble(1, a);
            stmt.setDouble(2, b);
            rs = stmt.executeQuery();
            while (rs.next()) {
                double buyPrice = rs.getDouble("buyPrice");
                System.out.println("Pret vanzare in intervalul dorit de noi:" + buyPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProduct(String a, String b) {
        Connection con = null;
        try {
            //Ne conectam la baza de date
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=SDA131208&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection succesful");
            PreparedStatement stmt = con.prepareStatement("UPDATE  products SET productName = ? WHERE productCode =? ");
            stmt.setString(1, a);
            stmt.setString(2, b);
            int size = stmt.executeUpdate();// INSERT, UPDATE, DELETE
            stmt.close();
            if (size > 0) {
                System.out.println("Update Done " + size);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertProduct(String a, String b, String c, String d, String j, String f, int g, double h, double i){
//ne conectam la bd
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=SDA131208&serverTimezone=UTC&characterEncoding=utf8");
            System.out.println("Connection succesful");
            PreparedStatement stmt = con.prepareStatement("INSERT INTO products values(?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, a);
            stmt.setString(2, b);
            stmt.setString(3, c);
            stmt.setString(4, d);
            stmt.setString(5, j);
            stmt.setString(6, f);
            stmt.setInt(7, g);
            stmt.setDouble(8,h);
            stmt.setDouble(9,i);
            int size = stmt.executeUpdate();
            stmt.close();
            if(size>0){
                System.out.println("Update done: " + size);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public boolean deleteProduct(String code) {
        String query = "DELETE FROM products WHERE productCode = ? ";
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels?user=root&password=SDA131208&serverTimezone=UTC&characterEncoding=utf8");
             PreparedStatement prepStat = conn.prepareStatement(query)) {
            prepStat.setString(1,code);
            int size = prepStat.executeUpdate();
            if (size > 0)
                return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}