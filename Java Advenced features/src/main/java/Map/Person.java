package Map;

import RecapitulareOOP.Staff;
import RecapitulareOOP.Student;

import java.util.HashMap;
import java.util.Map;

public class Person {
    public static void main(String[] args) {
        Map<String, RecapitulareOOP.Person> persons = new HashMap<>();
        RecapitulareOOP.Person man1 = new Student("Masterand", 1,5000,"Andrei","Bucuresti");
        RecapitulareOOP.Person man2= new Staff("Cristi", "Timisoara", "Medicina",60000);
        persons.put("person1", man1);
        persons.put("person2", man2);
        System.out.println(persons);

//        for(Map.Entry<String,Person> person: persons.entrySet()){
//            String key = person.getKey();
//            Person value = person.getValue();
//            System.out.println(key + " "+ value);
//        }
    }
}
