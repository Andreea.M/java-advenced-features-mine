package Map;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> students = new HashMap<>();
        students.put("George", 8);
        students.put("Paul", 7);
        students.put("Ioana",4);
        students.put("Laura", 10);
        students.put("Andrei", 5);

        System.out.println("Lista perechilor key - valoare sunt: ");
        for(Map.Entry<String, Integer> student: students.entrySet()){
            System.out.println(student);
            String key = student.getKey();
            Integer value = student.getValue();
            System.out.printf("%s: %d\n", key, value);

        }
        System.out.print("lista notelor este: ");
        for(Integer nota: students.values()){
            System.out.print(nota+" ");
        }
        System.out.println();
        System.out.println("Nota Ioanei este:" + students.get("Ioana"));

    }
}
