package RecapitulareOOP;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Person man1 = new Student("Masterand", 1,5000,"Andrei","Bucuresti");
        Person man2= new Staff("Cristi", "Timisoara", "Medicina",60000);
        System.out.println(man1);
        System.out.println(man2);

        Person student = new Student("Student",2,4500);
        Person staff = new Staff("farmacie", 5300);
        System.out.println(student);
        System.out.println(staff);

        student.setName("Matei");
        student.setAddress("Cluj");
        staff.setName("Rares");
        staff.setAddress("Suceava");
        System.out.println(student);
        System.out.println(staff);

        printNameAddress(man1);
        printNameAddress(man2);
        printNameAddress(student);
        printNameAddress(staff);

        Student student1 = new Student("Programare",2, 1500, "Alex","Suceava");
        Student student2 = new Student("Arhitectura",1,1000, "Vasile", "Botosani");
        Student student3 = new Student("Filologie",2,5000, "Marian", "Bucuresti");

        Staff staff1 = new Staff("Rares", "Iasi", "Profesor", 1200);
        Staff staff2 = new Staff("Codrut", "Constanta", "Programator", 1500);
        Staff staff3 = new Staff("Gabriel", "Timisoara", "Constructor", 1000);


        List<Person> students = new ArrayList<Person>();
        List<Person> staffs = new ArrayList<Person>();

        students.add(student1);
        students.add(student2);
        students.add(student3);

        staffs.add(staff1);
        staffs.add(staff2);
        staffs.add(staff3);

        printStudents(students);
        printStaff(staffs);

        man1.f();
        man2.f();
    }
        public static void printNameAddress(Person person){
            System.out.println(person.getName() + " - "+ person.getAddress());
        }
    public static void printStudents(List<Person> students) {

        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }
    }

        public static void printStaff (List < Person > staffs) {

            for (Person obj : staffs) {
                System.out.println(obj);
            }
        }

    }