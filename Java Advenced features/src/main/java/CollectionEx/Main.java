package CollectionEx;

import java.util.Queue;
import java.util.*;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) {
        final Set<Integer> numbersSet1 = new HashSet<>(); // stworzenie instancji HashSet
        out.println(numbersSet1.isEmpty()); // true, Set ndoes not contain elements
        numbersSet1.add(1);
        numbersSet1.add(17);
        numbersSet1.add(3);
        numbersSet1.add(2);
        numbersSet1.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersSet1.forEach(out::println);

        out.println("-----------------------------");
        final Set<Integer> numbersSet2 = new TreeSet<>();
        numbersSet2.add(1);
        numbersSet2.add(3);
        numbersSet2.add(2);
        numbersSet2.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersSet2.forEach(out::println);
        out.println("-----------------------------");

        final Set<Integer> numbersSet3 = new LinkedHashSet<>();
        numbersSet3.add(1);
        numbersSet3.add(3);
        numbersSet3.add(2);
        numbersSet3.add(1); // Adding an item with a value that already exists - the item is NOT added again
        numbersSet3.forEach(out::println);

        out.println("-----------------------------");
        final List<String> names = new ArrayList<>();
        names.add("Alex");
        names.add("George");
        names.add("Maria");
        names.add("Alina");
        for (final String name : names) {
            out.println(name);
        }

        out.println("-----------------------------");
        final LinkedList<String> names2 = new LinkedList<>();
        names2.add(0, "Ioana");
        names2.add(0, "Darius");
        names2.add(1, "Rares");
        names2.add(1, "Larisa");
        for (final String name2 : names2) {
            out.println(name2);
        }
        out.println("-----------------------------");
        final Queue<String> events = new LinkedList<>();
        events.offer("ButtonClicked");
        events.offer("MouseMoved");
        out.println(events.element());//afiseaza primul element
        out.println(events.peek());//displaying the  first element
        out.println(events.offer("Keyboard"));//adauga element in coada
        for (final String event : events) {
            out.println(event);
        }
        out.println("-----------------------------");
        out.println(events.poll());// removing the first item from the queue and returning a value
        out.println(events.poll());// removing the first item from the queue again and returning the value
        out.println(events.remove());//sterge ultimul din coada
        out.println(events.isEmpty());// at this point the queue is empty

        out.println("-----------------------------");
        final Deque<Integer> deques = new ArrayDeque<>();
        deques.offerLast(2);
        deques.offerFirst(1);
        for (final Integer deque : deques) {
            out.println(deque);
        }
        out.println(deques.pollLast());//sterge ultimul element din coada
        out.println(deques.peekLast());//sterge ultimul element din coada, fara sa modifice structura
        for (final Integer deque : deques) {
            out.println(deque);
        }
        out.println("-----------------------");
        deques.addFirst(5);
        deques.offerLast(9);
        for (final Integer deque : deques) {
            out.println(deque);
        }
        out.println("-----------------------");
        deques.pollFirst();
        for (final Integer deque : deques) {
            out.println(deque);
        }
        out.println("------------------------");
        deques.getLast();
        for (final Integer deque : deques) {
            out.println(deque);
        }
        out.println("--------------------------");

        final Map<Integer, String> ageToNames = new HashMap<>(); // tworzenie HashMapy
        ageToNames.put(11, "Andrew"); // adding items
        ageToNames.put(22, "Michael");  // adding another pair
        ageToNames.put(33, "John");  // adding a third pair to the map
        ageToNames.remove(22);     // removing an item based on the key
        out.println(ageToNames.get(11)); // displaying the value based on the key 11 -> Andrew

        final Map<Integer, String> ageToNamess = new LinkedHashMap<>(); // creating LinkedHashMap
        ageToNamess.put(20, "Maggie");
        ageToNamess.put(40, "Kate");
        ageToNamess.put(30, "Anne");
        out.println(ageToNamess.size());
        out.println("-----------------------------!!");

        for (final Integer key : ageToNamess.keySet()) { // key iteration using keySet()
            out.println("Key is map: " + key);     // the order of the keys is always the same -> 20, 40, 30
        }

        for (final String value : ageToNamess.values()) {   // iteration over values using values()
            out.println("Value in map is: " + value); // the order of the values is always the same -> Maggie, Anne, Kate
        }

        for (final Map.Entry<Integer, String> ageToName : ageToNamess.entrySet()) { // iteration over pairs with entrySet()
            out.println("Value for key  " + ageToName.getKey() + " is " + ageToName.getValue());
  /* the result will always be the following 3 lines, in this exact order (results from the use of LinkedHashMap)
     Value for key 20 is Maggie
     Value for key 40 is Kate
     Value for key 30 is Anne
   */
        }
        out.println("---------------------------");

        final Map<Integer, Integer> numberToDigitsSum = new TreeMap<>();
        numberToDigitsSum.put(33, 6);
        numberToDigitsSum.put(19, 10);
        numberToDigitsSum.put(24, 6);
        numberToDigitsSum.forEach((key, value) -> out.println(key + " " + value));

        out.println("--HashMap-------------------");
        HashMap<String, String> capitalCities = new HashMap<String, String>();
        capitalCities.put("England", "London");
        capitalCities.put("Germany", "Berlin");
        capitalCities.put("Norway", "Oslo");
        capitalCities.put("USA", "Washington DC");
        out.println(capitalCities);
        out.println("Capitala Germaniei este: " + capitalCities.get("Germany"));
        capitalCities.remove("USA");
        out.println(capitalCities);
        out.println("Nr de tari ramase este: " + capitalCities.size());
        capitalCities.clear();
        out.println("Lista tarilor este:" + capitalCities + capitalCities.size());

        out.println("-----HashMaps Integers-------------");
        HashMap<String, Integer> people = new HashMap<String, Integer>();
        people.put("John", 32);
        people.put("Steve", 30);
        people.put("Angie", 33);
        for (String i : people.keySet()) {
            out.println("key: " + i + " ,value: " + people.get(i));
        }

        out.println("----Iterator---------------");
        ArrayList<String> cars = new ArrayList<String>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");
        Iterator<String> it = cars.iterator();
        out.println(it.next());
        out.println("-------------------------");
        while (it.hasNext()) {
            out.println(it.next());
        }

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(12);
        numbers.add(8);
        numbers.add(2);
        numbers.add(23);
        Iterator<Integer> it2 = numbers.iterator();
        while (it2.hasNext()) {
            Integer i = it2.next();
            if (i < 10) {
                it2.remove();
            }
        }
        out.println("Numerele mai mici decat 10 sunt: " + numbers);

        out.println("-----tema lista------------");
        List<String> colors = new ArrayList<>();
        colors.add(0, "red");
        colors.add(1, "blue");
        colors.add(2, "green");
        colors.add(3, "yellow");
        out.println();
        out.print("Lista nr 1 de culori este: ");
        for (String color : colors) {
            out.print(color + ", ");
        }
        out.println();
        out.println("---Se creaza lista nr 2 de culori---------");

        final Set<String> colors2 = new TreeSet<>();
        colors2.add("purple");
        colors2.add("pink");
        colors2.add("grey");
        colors2.add("orange");
        colors2.add("pink");
        colors2.add("white");
        out.println();
        out.print("Lista nr 2 de culori este: ");
        for (String color : colors2) {
            out.print(color + " ,");
        }
        colors2.remove("grey");
        out.println();
        for (String color : colors2) {
            out.print(color + " ,");
        }

        out.println();
        out.println("---------Doar culorile care incep cu litera P----------");
        Iterator<String> iterator1 = colors2.iterator();
        while (iterator1.hasNext()) {
            if (iterator1.next().startsWith("p")) {
                out.println(iterator1.next());
               }
         }

    }
}

