package advanced_features_coding.day1;

public class Day1 {
    /*toate cele 12 exercitii din primul set sunt pornite de aici*/
    public static void main(String[] args) {
        /*schimba aici numele programului pe care vrei sa-l executi*/
        Program program = new Exercitiu4();
        /*dam instanta de Program lui ProgramHandler pentru*/
        ProgramHandler handler = new ProgramHandler(program);
        /*ii executa functia void run(String userInput)*/
        handler.start(); /*pornim actiunea*/
    }
}
