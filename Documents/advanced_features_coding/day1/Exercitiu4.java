package advanced_features_coding.day1;

/*
Folosind sistemul creat la punctele 1-3 creați
un program care îi cere utilizatorului o frază,
apoi găsește cel mai lung și cel mai scurt cuvânt din fraza introdusă;
 */
public class Exercitiu4 implements Program {
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return "Introdu o propozitie";
    }

    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        /*Obtinem cuvintele intr-un array*/
        String[] cuvinte = userInput.split(" ");
        /*presupunem ca primul cuvant are lungimea minima*/
        String cuvantScurt = cuvinte[0];
        /*presupun ca primul cuvant are lungimea maxima*/
        String cuvantLung = cuvinte[0];
        for(String cuvant : cuvinte) { /*parcurgem toate cuvintele*/
            if(!cuvant.equals("")) { /*nu luam in calcul elementele
                                    dintre cuvinte*/
                if (cuvant.length() < cuvantScurt.length()) {
                    cuvantScurt = cuvant; /*avem un nou minim*/
                } else if (cuvant.length() > cuvantLung.length()) {
                    cuvantLung = cuvant; /*avem un nou maxim*/
                }
            }
        }
        /*afisam rezultatele*/
        System.out.println("Cel mai scurt cuvant este: " + cuvantScurt);
        System.out.println("Cel mai lung cuvant este: " + cuvantLung);
    }
    public String toString() {return "Ziua 1 - exercitiul 4";}
}
