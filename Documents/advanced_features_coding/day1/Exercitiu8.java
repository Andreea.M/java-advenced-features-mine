package advanced_features_coding.day1;
/*
Folosind sistemul creat la punctele 1-3 creați un program care îi cere utilizatorului un număr rațional și apoi afișează două numere a căror
fracție are ca rezultat numărul introdus;
 */
public class Exercitiu8 extends InputNumarRational {
    /*Procesam input de la utilizator si afisam rezultatul*/
    /*deoarece mostenim din InputNumarNatural primim direct input-ul sub
    forma unei variabile de tip int*/
    public void run(double n) {
        int numitor = 1; //aici salvam rezultatul
        while((int)n != n) { /*cat timp mai avem cifre dupa virgula*/
            numitor *= 10; /*adaugam un 0 la numitor*/
            n *= 10; /*mutam virgula la dreapta cu o pozitie*/
        }
        System.out.println(n + "/" + numitor); //afisam rezultatul
    }
    public String toString() {return "Ziua 1 - exercitiul 8";}
}
