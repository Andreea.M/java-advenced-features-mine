package advanced_features_coding.day1;

import java.util.Arrays;
/*
Folosind clasele create la exercițiile anterioare creați un program care îi cere utilizatorului
o expresie aritmetică simplă și apoi afișează rezultatul. Expresia trebuie să fie compusă doi
operanzi (numere întregi) și un operator (+ - * / %).
 */
public class Exercitiu11 implements Program {
    private double first = 0.0;
    private double second = 0.0;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return "Enter an expression";
    }
    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        try {
            /*
             " *" == oricate spatii consecutive, inclusiv nici unul
             "[0-9]+" == oricate cifre consecutive, minim una
             "\\." == caracterul punct '.'
             "(\\.[0-9]+){0,1} == un punct urmat de oricate cifre (minim una);
             ^ gruparea poate sa apara odata sau sa lipseasca
             "\\+" == caracterul plus '+'
             " *[0-9]+(\\.[0-9]+){0,1} *" == un numar cu virgula inconjurat de oricate spatii
             https://regex101.com/
             */
            if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *\\+ *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "\\+");
                add();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *- *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "-");
                sub();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *\\* *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "\\*");
                multiply();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} */ *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "/");
                div();
            }
        } catch(Exception e) {}
    }
    private void extractParameters(String userInput, String delim) {
        String[] operands = userInput.split(delim + "| "); /*delimitam dupa operator si spatiu*/
        double first = 0.0;
        double second = 0.0;
        boolean firstSet = false;
        for(int i = 0; i < operands.length; ++i) {
            if(operands[i].equals("") == false) {
                if (firstSet == false) {
                    first = Double.parseDouble(operands[i]);
                    firstSet = true;
                } else {
                    second = Double.parseDouble(operands[i]);
                }
            }
        }
        this.first = first;
        this.second = second;
    }
    private void add() {
        System.out.println(first + second);
    }
    private void sub() {
        System.out.println(first - second);
    }
    private void multiply() {
        System.out.println(first * second);
    }
    private void div() {
        if(second == 0.0) { /*evitam impartirea la 0*/
            return;
        }
        System.out.println(first / second);
    }
    public String toString() {return "Ziua 1 - exercitiul 11";}
}
