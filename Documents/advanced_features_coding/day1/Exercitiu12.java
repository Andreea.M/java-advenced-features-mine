package advanced_features_coding.day1;

import java.util.HashMap;

/*
Folosind clasele create la exercițiile anterioare creați un program care îi cere utilizatorului
parametrii care definesc o formă geometrică (ex: pentru pătrat este nevoie de lungimea laturii)
și apoi afișează perimetrul și suprafața acesteia. Programul ar trebui să ofere suport cel puțin
pentru formele: dreptunghi, pătrat, cerc. Utilizatorul ar trebui să selecteze pentru ce formă vrea
să calculeze aria și perimetrul înainte să introducă parametrii acesteia (ex: dreptunghiul necesită
lungime și lățime în timp ce pătratul necesită doar lungimea laturii);
 */
public class Exercitiu12 implements Program {

    /*
    Starea de executie a programului:
        0    --> Utilizatorul poate sa aleaga forma
        1    --> Utilizatorul a ales o forma necunoscuta
        >= 2 --> Utilizatorul a ales o forma corecta
     */
    private int state = 0;
    /*va contine instanta formei aleasa de utilizator*/
    private Forma forma;
    /*salvam inputul introdus de utilizator*/
    /*pentru al afisa in mesajul de eroare*/
    private String previousUserInput;
    /*stocam cate o instanta din fiecare forma implementata*/
    private HashMap<String, Forma> formeImplementate;

    public Exercitiu12() {
        this.forma = null; /*utilizatorul inca nu a ales nici o forma*/
        formeImplementate = new HashMap<>(); /*initializam structura in care salvam formele*/
        formeImplementate.put("patrat", new Patrat()); /*adaugam un patrat*/
        formeImplementate.put("dreptunghi", new Dreptunghi()); /*adaugam un dreptunghi*/
        formeImplementate.put("cerc", new Cerc()); /*adaugam un cerc*/
    }
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        if(state == 0) {
            return "Alege forma";/*in starea 0 utilizatorul poate sa aleaga forma*/
        } else if(state == 1) {
            return "Nu cunosc " + previousUserInput; /*in starea 1 a ales o forma necunoscuta*/
        } else {
            return forma.getDescription(); /*altfel descrierea este implementata de forma*/
        }
    }

    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        if(state == 2) { /*daca stim forma aleasa de utilizator*/
            forma.run(userInput); /*forma implementeaza interactiunea cu utilizatorul*/
            if(forma.finished()) { /*daca forma a terminat de discutat cu utilizatorul*/
                System.out.println("Perimetru: " + forma.perimetru()); /*afisam perimetrul*/
                System.out.println("Aria: " +  forma.aria()); /*afisam aria*/
                state = 0; /*resetam programul*/
            }
        } else if(formeImplementate.containsKey(userInput)) { /*utilizatorul a ales forma corect*/
            state = 2; /*trecem in starea urmatoare*/
            forma = formeImplementate.get(userInput); /*setam forma aleasa de utilizator*/
        } else { /*singura varianta ramasa este daca utilizatorul a ales forma gresit*/
            state = 1; /*trecem in starea urmatoare*/
            previousUserInput = userInput; /*tinem minte input-ul gresit pentru mesajul de eroare*/
        }
    }
    public String toString() {return "Ziua 1 - exercitiul 12";}
}
