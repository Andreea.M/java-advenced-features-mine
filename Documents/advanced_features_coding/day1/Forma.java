package advanced_features_coding.day1;

/*Interfata implementata de clasele Forme geometrice din Exercitiu12*/
public interface Forma extends Program {
    boolean finished(); /*returneaza true daca a terminat de luat input de la utilizator*/
    double perimetru(); /*returneaza perimetrul formei geometrice*/
    double aria(); /*returneaza aria formei geometrice*/
}