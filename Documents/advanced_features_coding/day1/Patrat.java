package advanced_features_coding.day1;
/*
Implementarea unui patrat pentru Exercitiu12
 */
public class Patrat implements Forma {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu lungimea laturii";
    /*Descriere pentru utilizator in cazul unui input gresit*/
    public static final String BAD_INPUT = " nu e o lungime";
    /*incepem cu functionarea normala*/
    private String description = Patrat.NORMAL_DESCRIPTION;
    /*lungimea laturii*/
    private double L;
    /*daca am terminat de luat input de la utilizator*/
    private boolean finish = false;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {return description;}
    public boolean finished(){
        if(finish) { /*daca am terminat de luat input de la utilizator*/
            finish = false; /*resetam instanta curenta pentru o noua executie*/
            return true; /*si returnam faptul ca am terminat de luat input de la utilizator*/
        }
        return finish; /*altfel: returnam daca am terminat de luat input de la utilizator*/
    }
    /*Procesam input de la utilizator si il salvam in variabila membru*/
    public void run(String userInput){
        try {
            L = Double.parseDouble(userInput); /*incercam sa extragem lungimea din String-ul primit*/
            /*daca operatia anterioara a esuat*/
            /*codul "sare" la clauza catch*/
            /*deci daca ajungem la linia urmatoare*/
            /*stim ca utilizatorul a introdus lungimea corect*/
            /*si actualizam descrierea in cazul in care anterior era eroare*/
            description = Patrat.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            /*daca am ajuns aici, utilizatorul a introdus lungimea gresit
            actualizam descrierea cu eroarea potrivita*/
            description = userInput + Patrat.BAD_INPUT;
            return;
        }
        finish = true; /*semnalam faptul ca am terminat de luat input de la utilizator*/
    }
    /*returnam suma lungimilor laturilor*/
    public double perimetru() {
        return 4 * L;
    }
    /*returnam suprafata patratului*/
    public double aria() {
        return L * L;
    }
}