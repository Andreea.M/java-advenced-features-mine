package advanced_features_coding.day1;
/*
Folosind clasele create la exercițiile anterioare creați un program care îi cere
utilizatorului un număr natural N și apoi afișează al N-lea element al șirului
lui Fibonacci. Acest șir de numere are ca prim element 0, al doilea element
este 1, apoi fiecare element este suma celor două elemente
anterioare: 0, 1, 1, 2, 3, 5, 8, 13, 21, etc...;
 */
public class Exercitiu7 extends InputNumarNatural {
    /*Procesam input de la utilizator si afisam rezultatul*/
    /*deoarece mostenim din InputNumarNatural primim direct input-ul sub
    forma unei variabile de tip int*/
    public void run(int n) {
        if(n < 1) { /*situatie de eroare*/
            description = "Need a positive number"; //setez mesajul
            return; //termin executia curenta
        }
        description = InputNumarNatural.NORMAL_DESCRIPTION; //caz de succes, setez mesajul corespunzator
        System.out.println(fib(n)); //afisam rezultatul
    }
    private int fib(int n) {
        //0 1 1 2 3 5 8 13 21
        if(n == 1) { //primul element al sirului lui Fibonacci este 0
            return 0;
        }
        int first = 0; //primul element
        int second = 1; //al doilea
        int i = 2; //indexul elementului curent
        int current = second; //elementul curent
        while(i < n) { //cat timp n-am ajuns la al Nlea element
            current = first + second; //elementul curent este suma celor 2 termeni anteriori
            first = second; //mut elementele la dreapta cu o pozitie
            second = current; //mut elementele la dreapta cu o pizitie
            i++; //mut indexul
        }
        return current; //returnez elementul gasit
    }
    public String toString() {return "Ziua 1 - exercitiul 7";}
}
