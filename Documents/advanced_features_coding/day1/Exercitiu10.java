package advanced_features_coding.day1;
/*
Folosind clasele create la exercițiile anterioare creați un program care îi cere
utilizatorului un număr natural N, apoi N numere raționale și afișează numerele
raționale introduse în ordinea inversă a introducerii lor;
 */
public class Exercitiu10 extends InputDoubleArray {
    /*Procesam input de la utilizator si afisam rezultatul*/
    /*deoarece mostenim din InputDoubleArray primim direct input-ul sub
    forma membrului list*/
    public void run() {
        /*parcurgem lista in ordine inversa*/
        for(int i = list.size() - 1; i >= 0; --i) {
            System.out.println(list.get(i)); /*afisam numarul curent*/
        }
    }
    public String toString() {return "Ziua 1 - exercitiul 10";}
}
