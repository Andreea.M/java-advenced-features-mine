package advanced_features_coding.day1;

/*descrie transferul de date dintre o clasa Program si un apelant*/
/*clasele Program vor fi clase care vor implementa solutia la un anumit exercitiu*/
/*apelantul este clasa ProgramHandler care trebuie sa schimbe informatii cu
  clasa de tip Program in doua scenarii:*/
public interface Program {
    String getDescription(); /*descrierea catre utilizator trebuie
    				sa mearga de la Program catre ProgramHandler*/
    void run(String userInput); /*datele de intrare introduse de utilizator trebuie
    				sa mearga de la ProgramHandler catre Program*/
}
