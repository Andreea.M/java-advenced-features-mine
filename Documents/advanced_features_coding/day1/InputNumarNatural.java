package advanced_features_coding.day1;

/*
Implementeaza interactiunea cu utilizatorul pentru programe care necesita un
numar natural ca input (ex: 5)
Afiseaza utilizatorului o eroare daca input-ul introdus nu este numar natural.
 */
public abstract class InputNumarNatural implements Program {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu un numar natural";
    /*Descriere pentru utilizator in cazul unui input gresit*/
    public static final String BAD_NUMBER_DESCRIPTION = " nu este un numar natural";
    /*Descriere pentru utilizator in cazul unui input numar negativ*/
    public static final String NUMBER_OUT_OF_RANGE_DESCRIPTION = " este mai mic decat 0";

    /*incepem cu functionarea normala*/
    protected String description = InputNumarNatural.NORMAL_DESCRIPTION;
    /*Afisam utilizatorului inainte sa-i cerem input*/
    public String getDescription() {
        return description;
    }

    /*Procesam input de la utilizator si afisam rezultatul*/
    public void run(String userInput) {
        int N; /*pentru numarul introdus de utilizator*/
        try {
            N = Integer.parseInt(userInput); /*incercam sa extragem numarul
                                            din variabila de tip String*/
            /*daca operatia anterioara a esuat*/
            /*codul "sare" la clauza catch*/
            /*deci daca ajungem la linia urmatoare*/
            /*stim ca utilizatorul a introdus un numar intreg*/
            /*si actualizam descrierea in cazul in care anterior*/
            /*era una dintre erori*/
            description = InputNumarNatural.NORMAL_DESCRIPTION;
        } catch (Exception e) {
            /*daca am ajuns aici, utilizatorul a introdus altceva decat un*/
            /*numar intreg; actualizam descrierea cu eroarea potrivita*/
            description = userInput + InputNumarNatural.BAD_NUMBER_DESCRIPTION;
            return;
        }
        if(N >= 0) { /*daca numarul este natural*/
            run(N); /*executam programul mostenitor*/
        } else { /*numarul este negativ*/
            /*actualizam descrierea cu eroarea potrivita*/
            description = userInput + InputNumarNatural.NUMBER_OUT_OF_RANGE_DESCRIPTION;
        }
    }

    /*programele cu input un numar natural pot mosteni din aceasta clasa si vor
    primi numarul introdus de utilizator ca parametru la functia run(int)*/
    public abstract void run(int userInput);
}
