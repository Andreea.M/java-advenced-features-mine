package advanced_features_coding.day3.ordonate;

import advanced_features_coding.day3.liste.LinkedList;

public class Coada implements StructuraOrdonata {
    private LinkedList data = new LinkedList();
    public boolean isEmpty() {
        return data.size() == 0;
    }

    public void push(Object elem) {
        data.adaugaElement(elem, data.size());
    }

    public Object pop() {
        Object rez = top();
        data.stergeElement(0);
        return rez;
    }

    public Object top() { return data.get(0); }

    public void list() {
        data.afiseazaListaElemente();
    }
}
