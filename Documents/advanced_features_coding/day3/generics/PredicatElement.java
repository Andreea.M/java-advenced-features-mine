package advanced_features_coding.day3.generics;

public interface PredicatElement {
    boolean propozitie(Object e);
}
