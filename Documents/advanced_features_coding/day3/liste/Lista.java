package advanced_features_coding.day3.liste;

import advanced_features_coding.day3.generics.ActiuneElement;
import advanced_features_coding.day3.generics.PredicatElement;

public interface Lista {
    void adaugaElement(Object e);
    void adaugaElement(Object e, int index);
    boolean areElement(Object e);
    void stergeElement(Object e);
    void stergeElement(int index);
    void afiseazaListaElemente();
    void parcurge(ActiuneElement actiune);
    void ataseaza(Lista sursa);
    void ataseaza(Lista sursa, int index);
    Lista filtreaza(PredicatElement predicat);
    Object get(int index);
    int size();
    boolean isEmpty();
}
