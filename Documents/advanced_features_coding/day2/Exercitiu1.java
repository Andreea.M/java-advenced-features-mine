package advanced_features_coding.day2;

import java.util.Scanner;

public class Exercitiu1 {
    public void run() {
        Scanner input = new Scanner(System.in);/*pentru input de la utilizator*/
        boolean playing = true; /*schimbam in false cand terminam de jucat*/
        int secret = (int)(Math.random() * 100); /*generam un numar de la 0 la 100*/
        while(playing) { /*cat timp inca jucam*/
            System.out.println("Take a guess"); /*afisam utilizatorului o descriere*/
            int guess = input.nextInt(); /*citim incercarea de la utilizator*/
            if(guess > secret) { /*daca incercarea e mai mare decat numarul nostru*/
                System.out.println("Too high"); /*ii dam un indiciu utilizatorului*/
            } else if(guess < secret) { /*daca incercarea e mai mica decat numarul nostru*/
                System.out.println("Too low"); /*ii dam un indiciu utilizatorului*/
            } else { /*daca utilizatorul a ghicit numarul nostru*/
                System.out.println("You guessed it"); /*afisam mesajul de victorie*/
                playing = false; /*si incheiem jocul*/
            }
        }
    }
}
