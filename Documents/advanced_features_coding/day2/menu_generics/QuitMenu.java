package advanced_features_coding.day2.menu_generics;

/*Implementarea unui meniu de iesire din aplicatie*/
public class QuitMenu implements Menu {

    /*afisam un mesaj la terminarea aplicatiei*/
    public void draw() { System.out.println("Bye!"); }

    /*returnam null pentru a termina aplicatia*/
    /*vezi clasa MenuHandler pentru a vedea mecanismul complet*/
    public Menu select() { return null; }

    /*numele meniului*/
    public String toString() {return "Quit";}
}
