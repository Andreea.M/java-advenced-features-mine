package advanced_features_coding.day2.menu_generics;
/*
Clasa care modeleaza un meniu care nu are o lista de submeniuri
ci doar executa o actiune si returneaza meniul anterior
Pentru a folosi aceasta clasa o mostenim si implementam functia void run()
 */
public abstract class ActionMenu extends BaseMenu implements Runnable {
    public ActionMenu(Menu prev) {super(prev);}

    /*executam actiunea*/
    public void draw() {
        this.run();
    }

    /*returnam meniul anterior*/
    public Menu select() { return prev; }
}
