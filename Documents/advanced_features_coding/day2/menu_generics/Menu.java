package advanced_features_coding.day2.menu_generics;

/*interfata implementata de fiecare meniu*/
public interface Menu {
    /*functia de afisare a meniului*/
    void draw();
    /*functia in care cerem utilizatorului o optiune si*/
    /*construim meniul urmator in functie de aceasta*/
    Menu select();
}
