package advanced_features_coding.day2;

import advanced_features_coding.day2.ex4.MeniuPrincipal;
import advanced_features_coding.day2.menu_generics.MenuHandler;

public class Exercitiu4 {
    public static void main(String[] args) {
        /*instantiem un handler de meniuri*/
        /*incepand cu meniul principal*/
        MenuHandler handler = new MenuHandler(new MeniuPrincipal());
        handler.start(); /*incepem executia programului*/
    }
}
