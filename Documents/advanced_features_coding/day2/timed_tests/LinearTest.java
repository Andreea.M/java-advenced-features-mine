package advanced_features_coding.day2.timed_tests;

import advanced_features_coding.day2.TimeFunction;

/*Clasa care masoara durata parcurgerii unui array cu lungime configurabila*/
public class LinearTest extends TimeFunction {
    private long len;
    public LinearTest(long len) { this.len = len; }
    public void run() { for(long i = 0; i < len; ++i) {} }
    public String toString() { return "Linear " + len + "\t"; }
}
