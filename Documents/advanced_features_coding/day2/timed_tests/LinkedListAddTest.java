package advanced_features_coding.day2.timed_tests;

import java.util.LinkedList;

/*AddTest pe un LinkedList*/
public class LinkedListAddTest extends AddTest {
    public LinkedListAddTest(int times) {
        super(new LinkedList<>(), times);
    }
    public String toString() {
        return "LinkedListAddTest(" + times + ")";
    }
}
