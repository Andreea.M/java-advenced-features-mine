package advanced_features_coding.day2.timed_tests;

import java.util.List;

/*
Clasa care masoara durata adaugarii unui element pe
prima pozitie intr-o lista in mod repetat cu un numar
configurabil de repetitii
 */
public class AddTest extends ListTest {
    protected int times; /*numarul de repetitii*/
    public AddTest(List<Integer> list, int times) {
        super(list);
        this.times = times;
    }
    public void run() {
        for(int i = 0; i < times; ++i) {
            list.add(0);
        }
    }
}
