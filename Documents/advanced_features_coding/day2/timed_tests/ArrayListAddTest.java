package advanced_features_coding.day2.timed_tests;

import java.util.ArrayList;

/*AddTest pe un ArrayList*/
public class ArrayListAddTest extends AddTest {
    public ArrayListAddTest(int times) {
        super(new ArrayList<>(), times);
    }
    public String toString() {
        return "ArrayListAddTest(" + times + ") ";
    }
}
