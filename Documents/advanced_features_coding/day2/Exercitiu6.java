package advanced_features_coding.day2;


import advanced_features_coding.day2.ex6.HandlerListaNumere;

public class Exercitiu6 {
    public static void main(String[] args) {
        /*instantiem un handler al unei liste de numere*/
        HandlerListaNumere program = new HandlerListaNumere();
        program.handler.start(); /*incepem executia programului*/
    }
}
