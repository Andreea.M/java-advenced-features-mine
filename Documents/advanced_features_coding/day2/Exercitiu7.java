package advanced_features_coding.day2;


import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.MeniuPrincipal;
import advanced_features_coding.day2.menu_generics.MenuHandler;

public class Exercitiu7 {
    public static void main(String[] args) {
        /*instantiem un handler de meniuri*/
        /*incepand cu meniul principal pentru care instantiem si o biblioteca*/
        MenuHandler handler = new MenuHandler(new MeniuPrincipal(new Biblioteca()));
        handler.start(); /*incepem executia programului*/
    }
}
