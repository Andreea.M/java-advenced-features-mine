package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuInfo extends BaseMenu {
    public MeniuInfo(Menu prev) {
        super(prev);
        addOption("1", new MeniuCalcul(this)); /*submeniu probleme de calcul*/
        addOption("2", new MeniuStructuri(this)); /*submeniu structuri de date*/
        addOption("3", new MeniuAlgoritmi(this)); /*submeniu algoritmi*/
    }
    /*numele meniului*/
    public String toString() {
        return "Probleme de informatica";
    }
}
