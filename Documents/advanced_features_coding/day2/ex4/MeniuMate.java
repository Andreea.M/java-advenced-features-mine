package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuMate extends BaseMenu {
    public MeniuMate(Menu prev) {
        super(prev);
        addOption("1", new MeniuLogica(this)); /*submeniu logica*/
        addOption("2", new MeniuAnaliza(this)); /*submeniu analiza*/
        addOption("3", new MeniuDiferentiale(this)); /*submeniu
                                                    ecuatii diferentiale*/
    }
    /*numele meniului*/
    public String toString() {
        return "Probleme de matematica";
    }
}
