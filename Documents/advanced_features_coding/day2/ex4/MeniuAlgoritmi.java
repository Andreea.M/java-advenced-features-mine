package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuAlgoritmi extends BaseMenu {
    public MeniuAlgoritmi(Menu prev) {
        super(prev);
        /*lista exercitiilor cu algoritmi*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme cu algoritmi"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme cu algoritmi"));
    }
    /*numele meniului*/
    public String toString() {
        return "Algoritmica";
    }
}
