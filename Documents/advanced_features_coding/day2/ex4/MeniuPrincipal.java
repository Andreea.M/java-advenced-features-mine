package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;

import java.util.HashMap;

public class MeniuPrincipal extends BaseMenu {
    public MeniuPrincipal() {
        addOption(new MeniuMate(this)); /*submeniu mate*/
        addOption(new MeniuInfo(this)); /*submeniu info*/
    }
    /*numele meniului*/
    public String toString() { return "Meniu principal"; }
}
