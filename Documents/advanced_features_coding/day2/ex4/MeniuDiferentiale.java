package advanced_features_coding.day2.ex4;

import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuDiferentiale extends BaseMenu {
    public MeniuDiferentiale(Menu prev) {
        super(prev);
        /*lista problemelor cu ecuatii diferentiale*/
        addOption("1", new MeniuAfiseazaText(this, "Problema 1",
                "Enuntul primei probleme cu ecuatii diferentiale"));
        addOption("2", new MeniuAfiseazaText(this, "Problema 2",
                "Enuntul celei de-a doua probleme cu ecuatii diferentiale"));
    }
    /*numele meniului*/
    public String toString() {
        return "Ecuatii diferentiale";
    }
}
