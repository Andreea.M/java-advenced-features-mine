package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.BaseMenu;

public class MeniuPrincipal extends BaseMenu {
    HandlerListaNumere app;
    public MeniuPrincipal(HandlerListaNumere app) {
        this.app = app;
        addOption("adauga", new MeniuAdaugaNumar(this, app));
        addOption("afiseaza", new MeniuAfiseazaLista(this, app));
        addOption("schimba", new MeniuSchimbaNumar(this, app));
        addOption("sterge", new MeniuStergeNumar(this, app));
        addOption("salveaza", new MeniuSalveazaFisier(this, app));
        addOption("incarca", new MeniuIncarcaFisier(this, app));
        addOption("suma", new MeniuAfiseazaSuma(this, app));
        addOption("media", new MeniuAfiseazaMedia(this, app));
        addOption("max", new MeniuAfiseazaMax(this, app));
        addOption("min", new MeniuAfiseazaMin(this, app));
        addOption("crescator",
                new MeniuAfiseazaOrdonat(this, app,true));
        addOption("descrescator",
                new MeniuAfiseazaOrdonat(this, app,false));


    }

    public String toString() {
        return "Gestiune numere";
    }
}
