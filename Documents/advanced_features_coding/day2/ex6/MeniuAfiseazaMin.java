package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*Afisam numarul minim si ne intoarcem la meniul principal*/
public class MeniuAfiseazaMin extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAfiseazaMin(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println(app.min());
    }
    public String toString() {
        return "Afiseaza minim";
    }
}
