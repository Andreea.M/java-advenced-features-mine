package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Afisam lista sortata in functie de criteriul primit ca parametru la instantiere
Ne intoarcem la meniul principal
 */
public class MeniuAfiseazaOrdonat extends ActionMenu {
    HandlerListaNumere app;
    boolean crescator;
    public MeniuAfiseazaOrdonat(Menu prev, HandlerListaNumere app,
                                boolean crescator) {
        super(prev);
        this.app = app;
        this.crescator = crescator;
    }
    public void run() {
        app.afisareSortata(crescator);
    }
    public String toString() {
        return "Afiseaza " +
                (crescator ? "crescator" : "descrescator");
    }
}
