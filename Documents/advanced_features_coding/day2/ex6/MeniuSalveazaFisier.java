package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;
/*
Citim numele fisierului de la utilizator
Salvam lista in fisier
Ne intoarcem la meniul anterior
 */
public class MeniuSalveazaFisier extends ActionMenu {
    HandlerListaNumere app;
    public MeniuSalveazaFisier(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Numele fisierului:");
        app.salveaza(BaseMenu.input.nextLine());
    }
    public String toString() {
        return "Salveaza intr-un fisier";
    }
}