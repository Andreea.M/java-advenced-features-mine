package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Citim de la utilizator pozitia numarului pe care vrem sa-l schimbam si noua valoare
Schimbam numarul de la pozitia citita cu noua valoare
Ne intoarcem la meniul anterior
*/
public class MeniuSchimbaNumar extends ActionMenu {
    HandlerListaNumere app;
    public MeniuSchimbaNumar(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        boolean failedRead = false;
        do {
            failedRead = false;
            System.out.println("Introdu pozitia apoi noul numar");
            try {
                app.schimbaNumar(BaseMenu.input.nextInt(),
                        BaseMenu.input.nextInt());
            } catch(Exception e) {
                failedRead = true;
            } finally {
                BaseMenu.input.nextLine(); //functia nextInt lasa
                //caracterul newline "\n"
                //in consola si urmatorul apel
                //de nextLine() va returna "\n"
            }
        } while(failedRead);

    }
    public String toString() {
        return "Schimba un numar";
    }
}
