package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*Afisam numarul maxim si ne intoarcem la meniul principal*/
public class MeniuAfiseazaMax extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAfiseazaMax(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println(app.max());
    }
    public String toString() {
        return "Afiseaza maxim";
    }
}
