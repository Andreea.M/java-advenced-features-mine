package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Citim un numar de la utilizator
Il adaugam in lista
Ne intoarcem la meniul principal
 */
public class MeniuAdaugaNumar extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAdaugaNumar(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Introdu numarul:");
        app.adaugaNumar(BaseMenu.input.nextInt());
        BaseMenu.input.nextLine(); //functia nextInt lasa
                                //caracterul newline "\n"
                                //in consola si urmatorul apel
                                //de nextLine() va returna "\n"
    }
    public String toString() {
        return "Adauga un numar";
    }
}
