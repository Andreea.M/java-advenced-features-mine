package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*Afisam suma numerelor si ne intoarcem la meniul principal*/
public class MeniuAfiseazaSuma extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAfiseazaSuma(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println(app.suma());
    }
    public String toString() {
        return "Afiseaza suma";
    }
}
