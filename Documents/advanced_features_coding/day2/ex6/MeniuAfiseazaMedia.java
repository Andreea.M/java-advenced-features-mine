package advanced_features_coding.day2.ex6;

import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*Afisam media numerelor si ne intoarcem la meniul principal*/
public class MeniuAfiseazaMedia extends ActionMenu {
    HandlerListaNumere app;
    public MeniuAfiseazaMedia(Menu prev, HandlerListaNumere app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println(app.media());
    }
    public String toString() {
        return "Afiseaza media";
    }
}
