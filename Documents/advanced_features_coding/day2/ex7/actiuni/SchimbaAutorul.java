package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.SchimbareCarte;

public class SchimbaAutorul implements SchimbareCarte {
    private String autor;
    public SchimbaAutorul(String autor) {
        this.autor = autor;
    }
    public void schimba(Carte carte) {
        carte.setAutor(autor);
    }
}
