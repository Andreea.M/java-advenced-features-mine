package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;

public class DataPublicatieiEgalCu implements PredicatCarte {
    private String published;

    public DataPublicatieiEgalCu(String published) {
        this.published = published;
    }

    public boolean propozitie(Carte carte) {
        if(published.equals(Carte.formatImplicit.format(carte.getPublished()))) {
            return true;
        }
        return false;
    }
}
