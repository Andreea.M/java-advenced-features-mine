package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.SchimbareCarte;

public class SchimbaTitlul implements SchimbareCarte {
    private String titlu;
    public SchimbaTitlul(String titlu) {
        this.titlu = titlu;
    }
    public void schimba(Carte carte) {
        carte.setTitlu(titlu);
    }
}
