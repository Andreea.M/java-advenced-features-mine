package advanced_features_coding.day2.ex7.actiuni;

import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;

public class EdituraEgalCu implements PredicatCarte {
    private String editura;
    public EdituraEgalCu(String editura) {
        this.editura = editura;
    }
    public boolean propozitie(Carte carte) {
        if(editura.equals(carte.getEditura())) {
            return true;
        }
        return false;
    }
}
