package advanced_features_coding.day2.ex7.meniuri.principal.cautari;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuCautaDataPublicatiei extends BaseMenu {
    Biblioteca app;
    public MeniuCautaDataPublicatiei(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        addOption("la", new MeniuCautaLaDataPublicatiei(this, app));
        addOption("inainte",
                new MeniuCautaInainteDeDataPublicatiei(this, app));
        addOption("dupa", new MeniuCautaDupaDataPublicatiei(this, app));
    }

    public String toString() {
        return "Dupa data publicatiei";
    }
}
