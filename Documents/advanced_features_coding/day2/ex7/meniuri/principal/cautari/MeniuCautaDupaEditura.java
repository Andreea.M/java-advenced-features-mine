package advanced_features_coding.day2.ex7.meniuri.principal.cautari;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.MeniuAfiseazaLista;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuCautaDupaEditura extends BaseMenu {
    Biblioteca app;
    Biblioteca rezultate;
    public MeniuCautaDupaEditura(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        this.rezultate = new Biblioteca();
    }
    public void draw() {
        System.out.println("Editura:");
        rezultate = new Biblioteca(
                app.cautaDupaEditura(BaseMenu.input.nextLine())
        );
    }
    public Menu select() {
        return new MeniuAfiseazaLista(prev, rezultate);
    }

    public String toString() {
        return "Dupa editura";
    }
}
