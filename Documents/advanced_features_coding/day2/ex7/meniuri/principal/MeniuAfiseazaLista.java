package advanced_features_coding.day2.ex7.meniuri.principal;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuAdaugaCarte;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuDetaliiCarte;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.MeniuDetaliiCarteExtra;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

/*
Afiseaza lista sub forma unor submeniuri
Accesarea fiecarui submeniu corespunzator unei carti
afiseaza DOAR detaliile cartii
*/
public class MeniuAfiseazaLista extends BaseMenu {
    Biblioteca app;
    Menu meniuAdaugare;
    public MeniuAfiseazaLista(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
        meniuAdaugare = new MeniuAdaugaCarte(this, app);
    }
    public void draw() {
        removeCustomOptions();
        for(int i = 0; i < app.numarCarti(); ++i) {
            addOption(Integer.toString(i), new MeniuDetaliiCarte(this, app, i));
        }
        super.draw();
    }
    public String toString() {
        return "Afiseaza cartile";
    }
}
