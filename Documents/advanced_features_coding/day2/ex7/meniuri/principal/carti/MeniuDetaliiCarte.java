package advanced_features_coding.day2.ex7.meniuri.principal.carti;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaAutorul;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaDataPublicatiei;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaEditura;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaTitlul;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuDetaliiCarte extends BaseMenu {
    private Biblioteca app;
    private int index;
    public MeniuDetaliiCarte(Menu prev, Biblioteca app, int index) {
        super(prev);
        this.app = app;
        this.index = index;
    }

    public void draw() {
        System.out.println(app.getCarte(index));
        super.draw();
    }

    public String toString() {
        return app.getCarte(index).getTitlu();
    }
}
