package advanced_features_coding.day2.ex7.meniuri.principal.carti;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.Carte;
import advanced_features_coding.day2.menu_generics.ActionMenu;
import advanced_features_coding.day2.menu_generics.BaseMenu;
import advanced_features_coding.day2.menu_generics.Menu;

import java.util.Date;

public class MeniuAdaugaCarte extends ActionMenu {
    Biblioteca app;
    public MeniuAdaugaCarte(Menu prev, Biblioteca app) {
        super(prev);
        this.app = app;
    }
    public void run() {
        System.out.println("Titlu:");
        String titlu = BaseMenu.input.nextLine();
        System.out.println("Autor:");
        String autor = BaseMenu.input.nextLine();
        System.out.println("Editura:");
        String editura = BaseMenu.input.nextLine();
        Date published = null;
        boolean badDate;
        do {
            badDate = false;
            System.out.println("Data publicarii (yyyy-MM-dd):");
            try {
                published = Carte.formatImplicit.parse(BaseMenu.input.nextLine());
            } catch(Exception e) {
                System.err.println("Data necunoscuta");
                badDate = true;
            }
        } while(badDate);
        app.adauga(new Carte(titlu, autor, editura, published));
    }
    public String toString() {
        return "Adauga o carte";
    }
}
