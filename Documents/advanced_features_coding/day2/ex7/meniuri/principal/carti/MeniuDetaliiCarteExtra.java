package advanced_features_coding.day2.ex7.meniuri.principal.carti;

import advanced_features_coding.day2.ex7.Biblioteca;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaAutorul;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaDataPublicatiei;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaEditura;
import advanced_features_coding.day2.ex7.meniuri.principal.carti.schimbari.MeniuSchimbaTitlul;
import advanced_features_coding.day2.menu_generics.Menu;

public class MeniuDetaliiCarteExtra extends MeniuDetaliiCarte {
    public MeniuDetaliiCarteExtra(Menu prev, Biblioteca app, int index) {
        super(prev, app, index);
        addOption("st", new MeniuSchimbaTitlul(this, app, index));
        addOption("sa", new MeniuSchimbaAutorul(this, app, index));
        addOption("se", new MeniuSchimbaEditura(this, app, index));
        addOption("sd", new MeniuSchimbaDataPublicatiei(this, app, index));
        addOption("del", new MeniuStergeCarte(prev, app, index));
    }
}
