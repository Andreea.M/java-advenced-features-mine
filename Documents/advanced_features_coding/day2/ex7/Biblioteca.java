package advanced_features_coding.day2.ex7;

import advanced_features_coding.day2.ex7.actiuni.*;
import advanced_features_coding.day2.ex7.interfete_carte.PredicatCarte;
import advanced_features_coding.day2.ex7.interfete_carte.SchimbareCarte;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

/*
Clasa care ii permite utilizatorului sa gestioneze o lista cu carti
*/
public class Biblioteca {
    /*lista cu carti*/
    private ArrayList<Carte> lista = new ArrayList<>();

    /*constructor implicit*/
    public Biblioteca() {}
    /*constructor cu lista externa*/
    public Biblioteca(ArrayList<Carte> carti) {
        this.lista = carti;
    }

    /*pentru adaugarea unei carti*/
    public void adauga(Carte carte) {
        lista.add(carte);
    }
    /*pentru adaugarea unei carti pornind de la detaliile acesteia*/
    public void adauga(String detalii) {
        String[] elemente = detalii.split("[ ,]"); /*separam detaliile*/
        Carte noua = new Carte(); /*instantiem o carte noua*/
        for(String e : elemente) { /*pentru fiecare detaliu*/
            if(!e.isEmpty()) { /*care are continut*/
                if (noua.getTitlu() == null) { /*intai setam titlul*/
                    noua.setTitlu(e);
                } else if(noua.getAutor() == null) { /*apoi autorul*/
                    noua.setAutor(e);
                } else if(noua.getEditura() == null) { /*apoi editura*/
                    noua.setEditura(e);
                } else if(noua.getPublished() == null) { /*la final setam data*/
                    new SchimbaDataPublicatiei(e).schimba(noua);
                }
            }
        }
        adauga(noua); /*si adaugam noua carte la lista*/
    }
    /*returnam cate carti avem in gestiune*/
    public int numarCarti() {
        return lista.size();
    }

    /*pentru salvarea listei intr-un fisier*/
    public void salveaza(String numeFisier) {
        try {
            /*incercam sa deschidem fisierul pentru scriere*/
            BufferedWriter out = new BufferedWriter(new FileWriter(numeFisier));
            /*daca am ajuns aici am reusit sa deschidem fisierul*/
            for(int i = 0; i < lista.size(); ++i) { /*parcurgem lista*/
                Carte carte = lista.get(i);
                /*Serializam detaliile unei carti intr-un format pe care
                sa il putem decodifica usor (csv = Comma Separated Values)*/
                String output = carte.getTitlu() + "," +
                            carte.getAutor() + "," +
                            carte.getEditura() + "," +
                            Carte.formatImplicit.format(carte.getPublished()) + "\n";
                /*scriem cartea in fisier*/
                out.write(output);
            }
            /*inchidem fisierul*/
            out.close();
        } catch(Exception e) { /*daca nu am reusit sa deschidem fisierul*/
            System.err.println(numeFisier + " nu exista"); /*afisam o eroare*/
        }
    }
    /*pentru incarcarea listei dintr-un fisier*/
    public void incarca(String numeFisier) {
        try {
            /*incercam sa deschidem fisierul pentru citire*/
            Scanner in = new Scanner(new BufferedReader(new FileReader(numeFisier)));
            /*daca am ajuns aici am reusit sa deschidem fisierul*/
            lista.clear(); /*stergem cartile vechi*/
            while(in.hasNextLine()) { /*cat timp mai sunt carti necitite din fisier*/
                /*citim detaliile unei carti din fisier si o adaugam la lista*/
                adauga(in.nextLine());
            }
        } catch(Exception e) { /*daca nu am reusit sa deschidem fisierul*/
            System.err.println(numeFisier + " nu exista"); /*afisam o eroare*/
        }
    }

    /*pentru a schimba ceva la cartea de la pozitia index*/
    public void aplicaSchimbare(SchimbareCarte schimbare, int index) {
        if(index < 0 || index >= lista.size()) {
            System.err.println("Nu exista pozitia " + index);
        } else {
            schimbare.schimba(lista.get(index));
        }
    }
    /*pentru stergerea unei carti dupa index*/
    public void sterge(int index) {
        if(index < 0 || index >= lista.size()) {
            System.err.println("Nu exista pozitia " + index);
        } else {
            lista.remove(index);
        }
    }
    /*pentru afisarea detaliilor unei carti dupa index*/
    public void detalii(int index) {
        aplicaSchimbare(new SchimbareCarte() {
            public void schimba(Carte carte) {
                System.out.println(carte);
            }
        }, index);
    }
    /*pentru schimbarea titlului unei carti*/
    public void schimbaTitlul(String titlu, int index) {
        aplicaSchimbare(new SchimbaTitlul(titlu), index);
    }
    /*pentru schimbarea autorului unei carti*/
    public void schimbaAutorul(String autor, int index) {
        aplicaSchimbare(new SchimbaAutorul(autor), index);
    }
    /*pentru schimbarea editurii unei carti*/
    public void schimbaEditura(String editura, int index) {
        aplicaSchimbare(new SchimbaEditura(editura), index);
    }
    /*pentru schimbarea datei publicatiei unei carti*/
    public void schimbaDataPublicatiei(String published, int index) {
        aplicaSchimbare(new SchimbaDataPublicatiei(published), index);
    }
    /*pentru filtrarea listei dupa un anumit criteriu*/
    public ArrayList<Carte> cautaDupa(PredicatCarte cauta) {
        ArrayList<Carte> rezultate = new ArrayList<>();
        for(int i = 0; i < lista.size(); ++i) {
            if(cauta.propozitie(lista.get(i))) {
                rezultate.add(lista.get(i));
            }
        }
        return rezultate;
    }
    /*pentru filtrarea listei dupa titlu*/
    public ArrayList<Carte> cautaDupaTitlu(String titlu) {
        return cautaDupa(new TitlulEgalCu(titlu));
    }
    /*pentru filtrarea listei dupa autor*/
    public ArrayList<Carte> cautaDupaAutor(String autor) {
        return cautaDupa(new AutorulEgalCu(autor));
    }
    /*pentru filtrarea listei dupa editura*/
    public ArrayList<Carte> cautaDupaEditura(String editura) {
        return cautaDupa(new EdituraEgalCu(editura));
    }
    /*pentru filtrarea listei dupa data publicatiei (=)*/
    public ArrayList<Carte> cautaLaDataPublicatiei(String published) {
        return cautaDupa(new DataPublicatieiEgalCu(published));

    }
    /*pentru filtrarea listei dupa data publicatiei (<)*/
    public ArrayList<Carte> cautaInainteDeDataPublicatiei(String published) {
        try {
            return cautaDupa(new InainteDeDataPublicatiei(
                    Carte.formatImplicit.parse(published)
            ));
        } catch(Exception e) {
            return new ArrayList<>();
        }

    }
    /*pentru filtrarea listei dupa data publicatiei (>)*/
    public ArrayList<Carte> cautaDupaDataPublicatiei(String published) {
        try {
            return cautaDupa(new DupaDataPublicatiei(
                    Carte.formatImplicit.parse(published)
            ));
        } catch(Exception e) {
            return new ArrayList<>();
        }

    }

    /*pentru a obtine o carte de la un anumit index*/
    public Carte getCarte(int index) {
        return lista.get(index);
    }
}
