package advanced_features_day1;

import java.util.Scanner;

/*Administreaza executiile unui Program*/
public class ProgramHandler {
    private Program program; //programul al caror executii le vom administra
    private Scanner input = new Scanner(System.in); //folosim acest obiect pentru a
                                                //citi date de la utilizator
    public ProgramHandler(Program program) { //primim o instanta a unui Program concret
        this.program = program; //o salvam pentru a o folosi mai tarziu
    }
    public void start() { //apelam functia asta cand vrem sa incepem treaba
        String userInput = getUserInput(); //citesc user input odata
        while(userInput.equals("quit") == false) { //daca userul nu a zis "quit"
            program.run(userInput); //execut programul
            userInput = getUserInput(); //citesc user input pentru urmatoarea executie
        }
    }
    //functie helper pentru citirea datelor de intrare de la utilizator
    private String getUserInput() {
        System.out.println("Type \"quit\" to exit"); //Instructiuni iesire program
        System.out.println(program.getDescription()); //Afisez descrierea specifica programului
        return input.nextLine(); //citesc urmatoarea linie de la utilizator
    }
}
