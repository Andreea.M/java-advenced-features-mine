package advanced_features_day1;

public class FibonacciNthElementProgram extends IntegerInputProgram { //vezi clasa IntegerInputProgram
    /*userInput este numarul primit de la utilizator*/
    public void run(int userInput) {
        if(userInput < 1) { /*situatie de eroare*/
            description = "Need a positive number"; //setez mesajul
            return; //termin executia curenta
        }
        description = IntegerInputProgram.DESCRIPTION; //caz de succes, setez mesajul corespunzator
        if(userInput == 1) { //ni se cere fib(1)
            System.out.println(0); //afisam rezultatul
            return; //terminam executia curenta
        }
        int first = 0; //primul element
        int second = 1; //al doilea element
        int i = 2; //indexul elementului curent
        while(i < userInput) { //cat timp inca n-am ajuns la elementul cerut
            int current = first + second; //elementul curent este suma celor 2 termeni anteriori
            first = second; //mut elementele la dreapta cu o pozitie
            second = current; //mut elementele la dreapta cu o pizitie
            i++; //mut indexul
        }
        System.out.println(second); //returnez elementul gasit
    }
}
