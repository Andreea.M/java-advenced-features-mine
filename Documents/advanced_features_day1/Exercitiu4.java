package advanced_features_day1;

import java.util.Arrays;

/*
 * Gaseste cuvintele cu lungimea minima si maxima dintr-o fraza
 */
public class Exercitiu4 implements Program {

    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return "Introdu o fraza";
    }
    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        String[] cuvintele = userInput.split(" |\\.|,|!|\\?"); //"sparg" string-ul in substring-uri
                                                               //folosind ca delimitatori: ' ' '.' ',' '!' '?'
        System.out.println(Arrays.toString(cuvintele)); //verificam ce "cuvinte" am gasit
        String cuvantMaxim = cuvintele[0];              //presupun ca primul cuvant are lungimea maxima
        String cuvantMinim = cuvintele[0];              //presupun ca primul cuvant are lungimea minima
        for(String cuvant : cuvintele) { //parcurg toate cuvintele
            if(!cuvant.equals("")) { //nu iau in calcul "cuvintele" care se gasesc intre
                                     //doua semne de punctuatie consecutive
                if(cuvantMaxim.length() < cuvant.length()) { //avem un nou maxim
                    cuvantMaxim = cuvant;
                } else if(cuvantMinim.length() > cuvant.length()) { //avem un nou minim
                    cuvantMinim = cuvant;
                }
            }
        }
        //output catre user
        System.out.println(cuvantMaxim + " este cel mai lung cuvant cu " +
                    cuvantMaxim.length() + " litere ");
        System.out.println(cuvantMinim + " este cel mai scurt cuvant cu " +
                cuvantMinim.length() + " litere ");
    }
}
