package advanced_features_day1;

public class PalindromProgram implements Program {
    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return "Enter a word";
    }
    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        userInput = userInput.toLowerCase(); //nu vrem sa luam in calcul literele mari
        StringBuilder reverser = new StringBuilder(); //am nevoie de un StringBuilder pentru a inversa userInput
        reverser.append(userInput); //atasez continutul
        reverser.reverse(); //inversez continutul
        if(userInput.equals(reverser.toString())) { //daca userInput este egal cu inversul sau
            System.out.println(userInput + " este palindrom"); //userInput este palindrom
        } else { //daca userInput nu este egal cu inversul sau
            System.out.println(userInput + " nu este palindrom"); //userInput nu este palindrom
        }
    }
}
