package advanced_features_day1;


/*
 * Citeste un numar intreg, afiseaza-i cifrele in ordine inversa
 * IntegerReverser.java este alta implementare pentru aceeasi problema
 */
public class NumberReverser implements Program {
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static final String DESCRIPTION = "Enter an integer";
    /*Descriere pentru utilizator in cazul de input gresit*/
    public static final String BAD_INPUT = " is not a valid number";

    /*descrierea ce va fi afisata utilizatorului*/
    private String description = NumberReverser.DESCRIPTION;
    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return description;
    }
    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        int number = 0;
        try {
            number = Integer.parseInt(userInput);//arunca exceptie in caz de eroare
            description = NumberReverser.DESCRIPTION;//am citit N cu succes
        } catch(Exception e) {//sare aici daca userInput nu e numar
            description = userInput + NumberReverser.BAD_INPUT;//mesajul de eroare
            return;//termina executia curenta datorita erorii intalnite
        }
        int rez = 0; /*numarul inversat*/
        while(number != 0) { /*cat timp mai am cifre*/
            rez = rez * 10 + number % 10; /*adaug ultima cifra la rezultat*/
            number /= 10; /*elimin ultima cifra*/
        }
        System.out.println(rez); //afisez rezultatul
    }
}
