package advanced_features_day1;

/*O clasa abstracta are cel putin o metoda abstracta (fara implementare)*/
public abstract class IntegerInputProgram implements Program{
    /*Descriere pentru utilizator in cazul normal de functionare*/
    public static String DESCRIPTION = "Enter an integer";
    /*Descriere pentru utilizator in cazul de input gresit*/
    public static String BAD_INPUT = " is not a valid integer";

    /*descrierea ce va fi afisata utilizatorului*/
    protected String description = IntegerInputProgram.DESCRIPTION;
    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return description;
    }
    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        int number = 0;
        try {
            number = Integer.parseInt(userInput);//arunca exceptie in caz de eroare
            description = IntegerInputProgram.DESCRIPTION;//am citit N cu succes
        } catch(Exception e) {//sare aici daca userInput nu e numar
            description = userInput + IntegerInputProgram.BAD_INPUT;//mesajul de eroare
            return;//termina executia curenta datorita erorii intalnite
        }
        run(number); //am citit cu succes un int de la tastatura
                     //il dam mai departe urmatorului program spre a-l folosi
    }
    public abstract void run(int userInput);
}
