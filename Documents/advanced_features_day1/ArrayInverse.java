package advanced_features_day1;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Citeste de la tastatura un numar N
 * Apoi citeste N numere double
 * Afiseaza-le in ordinea inversa in care au fost introduse
 */
public class ArrayInverse implements Program{
    /*Descriere pentru utilizator inainte sa citesc N de la tastatura*/
    public static final String INPUT_N = "How many numbers do you have?";
    /*Descriere pentru utilizator in timp ce citesc cele N numere de la tastatura*/
    public static final String INPUT_NUMBERS = "Enter the next number";
    /*Descriere pentru utilizator pentru eroarea: N introdus gresit*/
    public static final String BAD_N = " does not reflect how many numbers you have";
    /*Descriere pentru utilizator pentru eroarea: unul din cele N numere a fost introdus gresit*/
    public static final String BAD_NUMBER = " is not a valid number";
    /*cand variabila state are aceasta valoare, urmeaza sa citesc N de la tastatura*/
    public static final int GETTING_N = -1;
    /*cand variabila state are aceasta valoarea, urmeaza sa citesc primul dintre cele N numere*/
    public static final int GETTING_FIRST_NUMBER = 0;

    /*descrierea ce va fi afisata utilizatorului*/
    private String description = ArrayInverse.INPUT_N;
    /*starea curenta*/
    private int state = ArrayInverse.GETTING_N;
    /*N numere*/
    private int n = 0;
    /*lista cu cele N numere*/
    private ArrayList<Double> list = new ArrayList<>();

    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return description;
    }

    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        if(state == ArrayInverse.GETTING_N) { /*daca userul tocmai l-a introdus pe N*/
            handleGettingN(userInput); /*salvez N in variabila membru*/
        } else if(state < this.n) { /*daca mai am de citit numere*/
            handleGettingNumber(userInput); /*adaug numarul in lista*/
        }
        if(state == this.n) { /*daca tocmai am citit ultimul numar*/
            handleOutput(); /*rezolvarea propriu zisa a problemei*/
        }
    }

    private void handleGettingN(String userInput) {
        int n = 0;
        try {
            n = Integer.parseInt(userInput); //arunca exceptie in caz de eroare
            description = ArrayInverse.INPUT_NUMBERS; //am citit N cu succes ==>
            state = ArrayInverse.GETTING_FIRST_NUMBER;  //trec in starea in care citesc cele N numere
            this.n = n;
        } catch (Exception e) { //daca userInput nu este un numar
            description = userInput + ArrayInverse.BAD_N; //setez descrierea pentru eroare
        }
    }

    private void handleGettingNumber(String userInput) {
        double nextNumber = 0.0;
        try {
            nextNumber = Double.parseDouble(userInput); //arunca exceptie in caz de eroare
            description = ArrayInverse.INPUT_NUMBERS; //am citit numarul cu succes
            list.add(nextNumber); //adaug numarul in lista
            state++; //cresc variabila de stare pentru a reflecta faptul ca am mai citit un numar
        } catch(Exception e) { //daca userInput nu este un numar
            description = userInput + ArrayInverse.BAD_NUMBER; //setez descrierea pentru eroare
        }
    }

    private void handleOutput() {
        for(int i = list.size() - 1; i >= 0; --i) { //parcurg lista in ordine inversa
            System.out.println(list.get(i)); //afisez fiecare element
        }
        description = ArrayInverse.INPUT_N; //resetez descrierea pentru executia urmatoare
        state = ArrayInverse.GETTING_N; //resetez starea pentru executia urmatoare
    }
}
