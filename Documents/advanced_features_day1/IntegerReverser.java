package advanced_features_day1;

/*
 * Citeste un numar intreg, afiseaza-i cifrele in ordine inversa
 * NumberReverser.java este alta implementare pentru aceeasi problema
 */
public class IntegerReverser extends IntegerInputProgram { //vezi clasa IntegerInputProgram
    /*userInput este numarul primit de la utilizator*/
    public void run(int userInput) {
        int rez = 0; /*numarul inversat*/
        while(userInput != 0) { /*cat timp mai am cifre*/
            rez = rez * 10 + userInput % 10; /*adaug ultima cifra la rezultat*/
            userInput /= 10; /*elimin ultima cifra*/
        }
        System.out.println(rez); //afisez rezultatul
    }
}
