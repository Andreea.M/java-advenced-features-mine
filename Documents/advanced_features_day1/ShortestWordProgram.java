package advanced_features_day1;

import java.util.Arrays;

public class ShortestWordProgram implements Program {
    public String getDescription() {
        return "Enter a sentence";
    }
    public void run(String userInput) {
        String[] words = userInput.split(" ");
        System.out.println(Arrays.toString(words));
        int min = words[0].length();
        int minIndex = 0;
        int max = words[0].length();
        int maxIndex = 0;
        for(int i = 1; i < words.length; ++i) {
            if(min > words[i].length()) {
                min = words[i].length();
                minIndex = i;
            }
            if(max < words[i].length()) {
                max = words[i].length();
                maxIndex = i;
            }
        }
        System.out.println("The shortest word is: " + words[minIndex]);
        System.out.println("The longest word is: " + words[maxIndex]);
    }
}
