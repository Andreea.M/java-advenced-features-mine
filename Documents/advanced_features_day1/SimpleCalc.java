package advanced_features_day1;

import java.util.Arrays;

public class SimpleCalc implements Program {
    private double first = 0.0;
    private double second = 0.0;
    public String getDescription() {
        return "Enter an expression";
    }
    public void run(String userInput) {
        try {
            if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *\\+ *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "\\+");
                add();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *- *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "-");
                sub();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} *\\* *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "\\*");
                multiply();
            } else if (userInput.matches(" *[0-9]+(\\.[0-9]+){0,1} */ *[0-9]+(\\.[0-9]+){0,1} *")) {
                extractParameters(userInput, "/");
                div();
            }
        } catch(Exception e) {}
    }
    private void extractParameters(String userInput, String delim) {
        String[] operands = userInput.split(delim + "| ");
        double first = 0.0;
        double second = 0.0;
        boolean firstSet = false;
        System.out.println("here " + Arrays.toString(operands));
        for(int i = 0; i < operands.length; ++i) {
            if(operands[i].equals("") == false) {
                if (firstSet == false) {
                    first = Double.parseDouble(operands[i]);
                    firstSet = true;
                } else {
                    second = Double.parseDouble(operands[i]);
                }
            }
        }
        this.first = first;
        this.second = second;
        System.out.println(first + " " + second + " <");
    }
    private void add() {
        System.out.println(first + second);
    }
    private void sub() {
        System.out.println(first - second);
    }
    private void multiply() {
        System.out.println(first * second);
    }
    private void div() {
        if(second == 0.0) {
            return;
        }
        System.out.println(first / second);
    }
}
