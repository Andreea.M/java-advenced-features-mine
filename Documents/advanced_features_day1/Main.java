package advanced_features_day1;

public class Main {

    public static void main(String[] args) {
        Program program = new FibonacciNthElementProgram(); /*schimba aici numele programului pe care vrei sa-l executi*/
        /*ex: Program program = new IntegerReverser(); */
        ProgramHandler handler = new ProgramHandler(program); /*dam instanta de Program lui ProgramHandler pentru*/
        							/*ii executa functia void run(String userInput)*/
        handler.start(); /*pornim actiunea*/
    }
}
