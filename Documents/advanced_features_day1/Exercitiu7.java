package advanced_features_day1;

/*
 * Afiseaza al N-lea element al sirului lui Fibonacci
 */
public class Exercitiu7 implements Program {
    /*mesaj pentru cazul normal de functionare*/
    public static final String NORMAL_DESCRIPTION = "Introdu un numar natural";
    /*mesaj pentru situatia in care datele de intrare nu reprezinta un numar*/
    public static final String BAD_NUMBER_DESCRIPTION = " is not a number";
    /*mesaj pentru situatia in care numarul introdus este mai mic de 1*/
    public static final String NUMBER_OUT_OF_RANGE_DESCRIPTION = " is less than 1";

    /*descrierea ce va fi afisata utilizatorului*/
    private String description = Exercitiu7.NORMAL_DESCRIPTION;
    /*returnez descrierea pentru utilizator catre ProgramHandler*/
    public String getDescription() {
        return description;
    }
    /*primesc input de la utilizator in acest parametru de tip String*/
    public void run(String userInput) {
        int N;
        try {
            N = Integer.parseInt(userInput); //arunca exceptie in caz de eroare
            description = Exercitiu7.NORMAL_DESCRIPTION; //am citit N cu succes
        } catch (Exception e) { //sare aici daca userInput nu e numar
            description = userInput + Exercitiu7.BAD_NUMBER_DESCRIPTION; //mesajul de eroare
            return; //termina executia curenta datorita erorii intalnite
        }
        if(N > 0) { //daca N este un numar de ordine
            System.out.println(fib(N)); //afiseaza rezultatul
        } else { //N <= 0
            description = userInput + Exercitiu7.NUMBER_OUT_OF_RANGE_DESCRIPTION; //mesajul de eroare
        }
    }

    private int fib(int n) {
        //0 1 1 2 3 5 8 13 21
        if(n == 1) { //primul element al sirului lui Fibonacci este 0
            return 0;
        }
        int first = 0; //primul element
        int second = 1; //al doilea
        int i = 2; //indexul elementului curent
        int current = second; //elementul curent
        while(i < n) { //cat timp n-am ajuns la al Nlea element
            current = first + second; //elementul curent este suma celor 2 termeni anteriori
            first = second; //mut elementele la dreapta cu o pozitie
            second = current; //mut elementele la dreapta cu o pizitie
            i++; //mut indexul
        }
        return current; //returnez elementul gasit
    }
}
