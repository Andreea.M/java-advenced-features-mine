package advanced_features_day1;

public class SimpleGreetingProgram implements Program {
    public String getDescription() {
        return "What's your name?";
    }
    public void run(String userInput) {
        System.out.printf("Hello %s, your name is %d letters\n",
                userInput, userInput.length());
    }
}
