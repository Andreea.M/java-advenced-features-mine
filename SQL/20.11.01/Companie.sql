employeesemployee-- create database CompanieX;
use companieX;

-- create table Companie(
-- Id int auto_increment primary key not null,
-- Denumire varchar(20),
-- Adresa varchar(20),
-- Descriere varchar(100)
-- );

-- create table Departament(
--  NrDep int auto_increment not null,
--  IdCompanie int not null,
--  DenumireDepartament varchar(20),
--  Adresa varchar(20),
--  primary key(NrDep),
--  constraint fk_departament_companie
--  foreign key (IdCompanie) references Companie(Id)
--  );

-- create table Proiect(
-- NrProiect int auto_increment not null,
-- Nume varchar(20),
-- Locatie varchar(20),
-- NrDep int not null,
-- OreProiect int,
-- CostProiect integer,
-- Primary Key (NrProiect),
-- constraint fk_Proiect_Departament
-- foreign key(NrDep) references Departament(NrDep)
-- );

-- create table Angajati(
-- CNP int not null auto_increment,
-- Nume varchar(20),
-- Prenume varchar(20),
-- Adresa varchar(30),
-- NrDep int not null,
-- primary key(CNP),
-- constraint fk_Angajati_Departament
-- foreign key(NrDep) references Departament(NrDep));

-- create table Activitate(
-- NrProiect int not null,
-- CNP int not null,
-- NrOre int,
-- primary key(NrProiect, CNP));

-- alter table Activitate 
-- add constraint fk_Activitate_Proiect
-- foreign key(NrProiect) references Proiect(NrProiect);

-- alter table Activitate
-- add constraint fk_Activitate_Angajati
-- foreign key(CNP) references Angajati(CNP);

-- insert into Companie(Denumire, Adresa, Descriere) 
-- value('CompaniaA', 'Cluj-Napoca','Cosmetice'),
-- ('CompaniaB', 'Bacau', 'Farmaceutice'),
-- ('CompaniaC', 'Iasi', 'ProduseBio');

-- insert into Departament
--  values(1, 1, 'Financiar','Str.Morii, nr33'),
--  (2,2, 'Productie','Str.Salcamilor,nr25'),
--  (3,3, 'Resurse Umane', 'Str.Veseliei,nr56');

-- insert into Proiect
-- values(1, 'X', 'Deva', 1, 15, 5000),
--  (2,'Y', 'Galati', 2, 10, 3500),
--  (3, 'Z', 'Mures', 3, 20, 7500);

-- insert into Angajati
-- values(190, 'Matei', 'Alexandru', 'Bucuresti', 1),
-- (288, 'Catana', 'Adelina', 'Cluj', 2),
-- (299, 'Albu', 'Madalina', 'Bacau', 2);

-- insert into Activitate
-- values(1, 288, 10),
-- (2, 190, 7),
-- (3, 299, 14);

-- Sa se afiseze toate companiile.
-- select Denumire from Companie;
-- select* from Companie;

-- Sa se afiseze toate departamentele
-- select DenumireDepartament from Departament;


-- Sa se afiseze toate departamentele care apartin companiei cu IdCompanie=1
-- select * from Departament where IdCompanie ='1';

-- Sa se afiseze toate proiectele.
-- select * from Proiect;

-- Sa se afiseze toate proiectele care au Costul mai mare decat 300
 -- select * from Proiect where CostProiect>'4000';

-- Sa se afiseze toate proiectele al caror numar de ore este intre  5 si 20.
-- select * from proiect where OreProiect>5 and OreProiect<20;

-- Sa se afiseze toti angajatii.
-- select * from Angajati;

-- Sa se afiseze toti angajatii care lucreaza in departamentul cu id = 2
-- select * from angajati where NrDep ='2';

-- Sa se afiseze CNP-ul angajatilor care lucreaza intr-un proiect.
-- select CNP from Activitate;


-- Sa se afiseze toti angajatii care lucreaza mai mult de 2 ore la proiectul cu nrProiect = 100
-- select * from activitate where NrOre>'2' and NrProiect = 2;

-- Sa se afiseze nrProiect pentru proiectele care au nrOre>20.
-- select count(NrProiect) from Activitate where NrOre>10;

-- Sa se afiseze toate inregistrarile din tabelul Activitate.
-- select * from Activitate;
