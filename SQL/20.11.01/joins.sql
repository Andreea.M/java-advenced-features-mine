use myFirstDatabase;

-- select * from owners,pet;
-- similar
-- select * from owners join pet;

-- select * from owners inner join pet on pet.ownerId = owners.ownerId;

-- select * from pet left join owners on pet.ownerId = owners.ownerId;

-- select * from pet right join owners on pet.ownerId = owners.ownerId;
