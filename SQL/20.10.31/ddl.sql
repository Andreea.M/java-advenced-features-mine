/*create database myFirstDatabase;*/
/*use myfirstdatabase;*/

/*create table owners (
firstName varchar(25) not null,
lastName varchar(25) not null);*/

/*alter table owners
add dataOfBirth date not null;*/

/*alter table owners
modify column dataOfBirth date unique;*/

/*alter table owners
drop column dataOfBirth;*/

/*drop table owners;*/

/*drop database myfirstdatabase;*/

/*slide 19*/
/*Create a new database:humanResources.
2.Create a new table employees, with the following columns:a.employeeId - Integer,b.firstName - Varchar,c.lastName - Varchar,d.dateOfBirth - Date,e.postalAddress - Varchar.
3.Alter table employees and add the following columns:a.phoneNumber - Varchar,b.email - Varchar,c.salary - Integer.
4.Alter table employees and remove the postalAddress column.
5.Create a new table employeeAddresses, with country - Varchar column.
6.Remove table employeeAddresses*/
/*create database humanResources;*/
/*use humanResources;*/
/*
create table employees(
employeeId integer not null unique auto_increment,
firstName varchar(20),
lastName varchar(20),
dataOfBirth date,
postalAddress varchar(10)
);
*/
/*alter table employees
add column phoneNumber varchar(10),
add column email varchar(20),
add column salary int;*/

/*alter table employees
drop column postalAddress;*/
/*
create table employeeAddresses(
country varchar(20)
);
*/

/*drop table employeeAddresses;*/







