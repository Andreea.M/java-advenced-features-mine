/*create database humanresource3;*/
use humanresource3;

/*create table employees(
employeeId int not null primary key auto_increment,
firstName varchar(20),
lastName varchar(20),
dateOfBirth date,
phonenumber varchar(17), 
email varchar(20),
salary int,
managerId int not null,
constraint fk_employees_employees
foreign key(managerId) references employees(employeeId));*/

/*create table departments(
departmentId int not null primary key auto_increment,
departmentName varchar(20));*/

/*create table projects(
projectId int not null primary key auto_increment,
projectName varchar(10),
description varchar(25));*/

/*alter table employees
add column departmentId int not null;*/

-- alter table employees
-- add constraint fk_employees_departments
-- foreign key (departmentId) references departments(departmentId);

-- create table employees_project(
-- EmployeeProjectId int not null auto_increment primary key,
-- EmployeeId int not null,
-- ProjectId int not null
-- );

-- alter table employees_project
-- add constraint fk_employees_project_employees
-- foreign key(employeeId) references employees(employeeId);

alter table employees_project
add constraint fk_employees_project_projects
foreign key(ProjectId) references projects(ProjectId);
