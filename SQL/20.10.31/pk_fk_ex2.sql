-- create database companiePK;

use companiePK;

-- create table Companie(
-- Id int auto_increment primary key not null,
-- Denumire varchar(20),
-- Adresa varchar(20),
-- Descriere varchar(100)
-- );

-- create table Departament(
--  NrDep int auto_increment not null,
--  IdCompanie int not null,
--  DenumireDepartament varchar(20),
--  Adresa varchar(20),
--  primary key(NrDep),
--  constraint fk_departament_companie
--  foreign key (IdCompanie) references Companie(Id)
--  );

-- create table Proiect(
-- NrProiect int auto_increment not null,
-- Nume varchar(20),
-- Locatie varchar(20),
-- NrDep int not null,
-- OreProiect int,
-- CostProiect integer,
-- Primary Key (NrProiect),
-- constraint fk_Proiect_Companie
-- foreign key(NrDep) references Companie(Id)
-- );

-- create table Angajati(
-- CNP int not null auto_increment,
-- Nume varchar(20),
-- Prenume varchar(20),
-- Adresa varchar(30),
-- NrDep int not null,
-- primary key(CNP),
-- constraint fk_Angajati_Companie
-- foreign key(NrDep) references Companie(Id));

-- create table Activitate(
-- NrProiect int not null auto_increment,
-- CNP int not null,
-- NrOre int,
-- primary key(NrProiect),
-- constraint fk_Activitate_Companie
-- foreign key(CNP) references Companie(Id));

-- insert into Companie(Denumire, Adresa, Descriere) 
-- value('CompaniaA', 'Cluj-Napoca','Cosmetice'),
-- ('CompaniaB', 'Bacau', 'Farmaceutice'),
-- ('CompaniaC', 'Iasi', 'ProduseBio');


/*insert into Angajati
values(290, 'Matei', 'Alexandru', 'Bucuresti', 1);*/

-- update Angajati 
-- set CNP='190'
-- where CNP='290';


-- insert into Angajati
-- values(288, 'Catana', 'Adelina', 'Cluj', 2);

-- insert into Angajati
-- values(299, 'Albu', 'Madalina', 'Bacau', 2);

-- insert into departament(IdCompanie, DenumireDepartament)
-- values(1, 'Financiar'),
-- (2, 'Productie'),
-- (3, 'Resurse Umane');

-- insert into departament(IdCompanie, Adresa)
-- values(1,'Str.Morii, nr33'),
-- (2,'Str.Fabricii, nr45'),
-- (3,'Str.Lunii, nr12');
 
--  delete from Departament;
 
 -- insert into Departament
--  values(1, 1, 'Financiar','Str.Morii, nr33'),
--  (2,2, 'Productie','Str.Salcamilor,nr25'),
--  (3,3, 'Resurse Umane', 'Str.Veseliei,nr56');
 
 -- insert into Proiect
--  values(1, 'X', 'Deva', 1, 15, 5000),
--  (2,'Y', 'Galati', 2, 10, 3500),
--  (3, 'Z', 'Mures', 3, 20, 7500);

insert into Activitate
values(1, 288, 10),
(2, 190, 7),
(3, 299, 14);