package day2.Ex3;

class Print implements Runnable{
    public void run(){
        System.out.println("hello");
    }
    public String toString() {
        return "Print";
    }
}

class ForLoop implements Runnable{
    public void run(){
        for(int i = 0; i< 100000; i++) {}
        for(int i = 0; i < 100000; ++i) {}
        for(int i = 0; i < 100000; ++i) {}
    }

    @Override
    public String toString() {
        return "ForLoop{}";
    }
}

class QuadraticLoop implements Runnable{
    public void run(){
        for(int i = 0; i< 100000; i++){
            for(int j = 0; j< 10000; j++){}
        }
    }
    @Override
    public String toString() {
        return "QuadraticLoop{}";
    }
}

class Timer {
    private Runnable action;
    public Timer(Runnable action) {
        this.action = action;
    }
    public long time() {
        long before = System.nanoTime();//execut secventa de cod;
        long after = System.nanoTime();
        long diff = after - before;
        System.out.println("Testing "  + action + " " + diff + " nanos");
        return diff;
    }
}

public class Exercitiul3 {
    public static void main(String[] args) {
    day2.Ex3.Timer t = new day2.Ex3.Timer(new ForLoop());
    t.time();
    t = new day2.Ex3.Timer(new QuadraticLoop());
    t.time();
    }
}

