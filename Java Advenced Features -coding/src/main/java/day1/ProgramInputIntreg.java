package day1;

public abstract class ProgramInputIntreg implements Program {
    public static final String NORMAL_DESCRIPTION = "Introdu un  numar natural";
    public static final String BAD_NUMBER_DESCRIPTION = "is not a number";
    public static final String NUMBER_OUT_OF_RANGE_DESCRIPTION =" is less than 1";

    private String description = ProgramInputIntreg.NORMAL_DESCRIPTION;
    public String getDescription() {
        return description;
    }
    public void run(String userInput) {
        System.out.println("The number (type String):" + userInput);
        int N;
        try{
            N = Integer.parseInt(userInput);
            description = ProgramInputIntreg.NORMAL_DESCRIPTION;
        }catch (Exception e){
            description = userInput + ProgramInputIntreg.BAD_NUMBER_DESCRIPTION;
            return;
        }
        if(N > 0){
            run(N);
        }else{
            description = userInput + ProgramInputIntreg.NUMBER_OUT_OF_RANGE_DESCRIPTION;
        }
    }
   public abstract void run(int userInput);
}
