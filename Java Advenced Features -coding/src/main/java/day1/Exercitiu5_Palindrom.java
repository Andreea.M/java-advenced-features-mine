package day1;

public class Exercitiu5_Palindrom implements Program{

    public void run(String userInput) {
        userInput = userInput.toLowerCase();
        StringBuilder userInputReverse = new StringBuilder();
        userInputReverse.append(userInput);
        userInputReverse.reverse();
        if (userInput.equals(userInputReverse.toString())) {
            System.out.println(userInput + " is palindrom");
        } else {
            System.out.println(userInput + " is't palindrom");
        }
    }

    public String getDescription() {
        return "Enter a word: ";
    }

//varianta mea
//    public void run(String userInput) {
//        System.out.println("Cuvantul introdus este: " + userInput );
//       String userInputG = userInput.toLowerCase();
//        char [] word = userInputG.toCharArray();
//        if(istPalindrom(word) == true){
//        System.out.println("Cuvantul "+ userInput + " este palindrom!");
//        }else{
//            System.out.println("Cuvantul " + userInput + " nu este palindrom!");
//        }
//    }
//    public static boolean istPalindrom(char[] word){
//        int i1 = 0;
//        int i2 = word.length - 1;
//        while (i2 > i1) {
//            if (word[i1] != word[i2]) {
//                return false;
//            }
//            ++i1;
//            --i2;
//        }
//        return true;
//    }


}
