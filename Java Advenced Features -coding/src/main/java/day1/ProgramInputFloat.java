package day1;

public abstract class ProgramInputFloat implements Program {
    public static final String NORMAL_DESCRIPTION = "Introdu un  numar real";
    public static final String BAD_NUMBER_DESCRIPTION = "is not a number";


    private String description = ProgramInputFloat.NORMAL_DESCRIPTION;
    public String getDescription() {
        return description;
    }
    public void run(String userInput) {
        System.out.println("The number (type String):" + userInput);
        float N;
        try{
            N = Float.parseFloat(userInput);
            description = ProgramInputFloat.NORMAL_DESCRIPTION;
        }catch (Exception e){
            description = userInput + ProgramInputFloat.BAD_NUMBER_DESCRIPTION;
            return;
        }

    }
    public abstract void run(float userInput);
}
