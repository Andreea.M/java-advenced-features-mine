package day1;

public interface Program {
    void run(String userInput);
    String getDescription();

}
