package day1;

public class GreetingsProgram implements Program{
    public void run(String userInput){
        System.out.println("Hello " + userInput + "!");
        System.out.println("Your name has " + userInput + "and has: " + userInput.length() + " letters");
    }
    public String getDescription(){
        return "What's your name?";
    }
}
