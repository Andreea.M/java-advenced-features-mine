package day1;

import java.util.Arrays;

public class Exercitiu4_Min_Max implements Program{

    public String getDescription() {
        return "Introdu o fraza";
    }
    public void run(String userInput) {
    String[] cuvintele = userInput.split(" |,|;|:|!|\\.");
        System.out.println(Arrays.toString(cuvintele));
        String cuvantMaxim = cuvintele[0];
        String cuvantMinim = cuvintele[0];
        for (String cuvant:cuvintele) {
            if (!cuvant.equals("")) {
                if (cuvantMaxim.length() < cuvant.length()) {
                    cuvantMaxim = cuvant;
                } else if (cuvantMinim.length() > cuvant.length()) {
                    cuvantMinim = cuvant;
                }
            }
        }
        System.out.println(cuvantMaxim + "  este cel mai lung cuvant din fraza si are lungimea maxima de: " + cuvantMaxim.length() + " litere");
        System.out.println(cuvantMinim + " este cel mai scurt cuvant din fraza si are lungimea minima de: " + cuvantMinim.length() + " litere");
    }
}
