package day1;

import java.util.Scanner;

public class Exercitiu12_Forme implements Program{
    public static Scanner scanner = new Scanner((System.in));

    public void run(String userInput) {
        Shape shape = null;
        if(userInput.equals("1")){
            System.out.println("print square");
            shape = new Square();
        }else if(userInput.equals("2")){
            System.out.println("print rectangle");
            shape = new Rectangle();
        }else if(userInput.equals("3")){
            System.out.println("print circle");
            shape = new Circle();

        }else{
            System.out.println("What is that?");
        return;}

        shape.requestParameter(scanner);
        System.out.println(shape.area());
        System.out.println(shape.perimeter());

    }

    public String getDescription() {
        return "Alege o forma geometrica:" +
                " 1-> square " +
                " 2-> rectangle " +
                " 3-> circle" ;
    }
}

interface Shape{
    double area();
    double perimeter();
    void requestParameter(Scanner scanner);
}

class Rectangle implements Shape{
    double l;
    double l2;
    public double area(){
    return l*l2;
    }

    public double perimeter(){
    return (l+l2)*2;
    }

    public void requestParameter(Scanner scanner){
    System.out.println("Enter two values: ");
    this.l = scanner.nextDouble();
    this.l2 = scanner.nextDouble();
   }

 }

  class Square implements Shape{
    double l;
    public double area(){
        return l*l;
    }
    public double perimeter(){
        return l*4;
    }
    public void requestParameter(Scanner scanner){
        System.out.println("Enter value: ");
        this.l = scanner.nextDouble();
    }


}
class Circle implements Shape{
    double l;
    public double area(){
        return Math.PI*l*l;
    }
    public double perimeter(){
        return l*2*Math.PI;
    }
    public void requestParameter(Scanner scanner){
        System.out.println("Enter value: ");
        this.l = scanner.nextDouble();
    }

}
