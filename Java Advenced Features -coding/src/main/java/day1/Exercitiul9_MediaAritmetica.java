package day1;

import java.util.ArrayList;

public class Exercitiul9_MediaAritmetica implements Program {
    public static final String INPUT_N = "Cate numere doriti sa introduceti?";
    public static final String INPUT_NUMBERS = "Introduceti urmatorul numar";
    public static final String BAD_N = "N introdus gresit";
    public static final String BAD_NUMBER = "unul dintre numere a fost introdus gresit.";
    public static final int GETTING_N = -1;
    public static final int GETTING_FIRST_NUMBER = 0;

    private String description = Exercitiul9_MediaAritmetica.INPUT_N;
    private int state = Exercitiul9_MediaAritmetica.GETTING_N;
    private int n = 0;
    private ArrayList<Double> list = new ArrayList<>();

    public String getDescription() {
        return description;
    }

    public void run(String userInput) {
        if(state == Exercitiul9_MediaAritmetica.GETTING_N){
            handleGetting(userInput);
        }else if(state< this.n){
            handleGettingNumber(userInput);
        }
        if(state == this.n){
            handleOutput();
        }
    }

    private void handleGetting(String userInput){
        int n = 0;
        try{
            n = Integer.parseInt(userInput);
            description = Exercitiul9_MediaAritmetica.INPUT_NUMBERS;
            state = Exercitiul9_MediaAritmetica.GETTING_FIRST_NUMBER;
            this.n = n;
        }catch (Exception e){
            description = userInput + Exercitiul9_MediaAritmetica.BAD_N;
        }
    }

    private void handleGettingNumber(String userInput){
        double nextNumber =0.0;
        try{
            nextNumber = Double.parseDouble(userInput);
            description = Exercitiul9_MediaAritmetica.INPUT_NUMBERS;
            list.add(nextNumber);
            state++;
        }catch (Exception e){
            description = userInput + Exercitiul9_MediaAritmetica.BAD_NUMBER;
        }
    }

    private void handleOutput(){
        double sum = 0.0;
        for(int i = 0; i< list.size(); ++i){
            sum += list.get(i);
        }
        System.out.println("Media aritmetica a numerelor introduse este: " + sum/list.size());

        System.out.println();
        description = Exercitiul9_MediaAritmetica.INPUT_N;
        state = Exercitiul9_MediaAritmetica.GETTING_N;

    }

}
