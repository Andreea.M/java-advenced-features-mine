package day1;

public class Exercitiu6_InvN extends ProgramInputIntreg {

    public void run(int userInput) {
        int inversN =0;
        while(userInput!=0){
            inversN= inversN*10 + userInput%10;
            userInput= userInput/10;
        }
        System.out.println("The reverse of the number is: " + inversN);
    }

    public String getDescription() {
        return "Enter a number: ";
    }
}
