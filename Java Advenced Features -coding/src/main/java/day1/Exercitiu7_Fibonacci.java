package day1;

public class Exercitiu7_Fibonacci implements Program {
    public static final String NORMAL_DESCRIPTION = "Introdu un  numar natural";
    public static final String BAD_NUMBER_DESCRIPTION = "is not a number";
    public static final String NUMBER_OUT_OF_RANGE_DESCRIPTION =" is less than 1";
    private String description = Exercitiu7_Fibonacci.NORMAL_DESCRIPTION;
    public String getDescription() {
        return description;
    }
    public void run(String userInput) {
        System.out.println(userInput);
        int N;
        try{
            N = Integer.parseInt(userInput);
            System.out.println("N = "+N);
            description = Exercitiu7_Fibonacci.NORMAL_DESCRIPTION;
        }catch (Exception e){
            description = userInput + Exercitiu7_Fibonacci.BAD_NUMBER_DESCRIPTION;
            return;
        }
        if(N > 0){
            System.out.println(fib(N));
        }else{
            description = userInput + Exercitiu7_Fibonacci.NUMBER_OUT_OF_RANGE_DESCRIPTION;
        }
    }
    private int fib(int n){
        if(n==1){
            return 0;
        }
        int first = 0;
        int second = 1;
        int current = second;
        int i = 2;
        while(i < n){
            current = first + second;
            first = second;
            second = current;
            i++;
        }
        return current;
    }
}
