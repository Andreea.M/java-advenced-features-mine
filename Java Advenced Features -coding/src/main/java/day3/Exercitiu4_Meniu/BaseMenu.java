package day3.Exercitiu4_Meniu;

import java.util.Scanner;

public abstract class BaseMenu implements Menu{

    protected Menu previous;
    protected Scanner input = new Scanner(System.in);

    public BaseMenu(Menu previous){
        this.previous= previous;
    }

}
