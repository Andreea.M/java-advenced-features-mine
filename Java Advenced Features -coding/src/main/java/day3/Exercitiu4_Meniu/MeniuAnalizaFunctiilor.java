package day3.Exercitiu4_Meniu;

public class MeniuAnalizaFunctiilor extends BaseMenu{

    public MeniuAnalizaFunctiilor(Menu previous){
        super(previous);
    }
    public void draw() {
        System.out.println("1 -> Problema 1");
        System.out.println("2 -> Problema 2");
        System.out.println("3 -> Problema 3");
        System.out.println("4 -> Meniu anterior! ");

    }
    public Menu select() {
        String userInput = input.nextLine();
        if (userInput.equals("1")) {
            System.out.println("Enuntul problemei 1");
            return this;
        } else if (userInput.equals("2")) {
            System.out.println("Enuntul problemei 2");
            return this;
        } else if (userInput.equals("3")) {
            System.out.println("Enuntul problemei 3");
            return this;
        } else if (userInput.equals("4")) {
            return previous;
        } else {
            System.out.println("Optiune invalida! ");
            return this;
        }
    }
}
