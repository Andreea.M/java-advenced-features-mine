package day3.Exercitiu4_Meniu;

public class MeniuInfo extends BaseMenu{
    public MeniuInfo(Menu previous){
        super(previous);
    }

    public void draw() {
        System.out.println("1 -> Probleme de calcul");
        System.out.println("2 -> Structuri de date");
        System.out.println("3 -> Algoritmica");
        System.out.println("4 -> Meniu anterior");
    }

    public Menu select() {
        String userInput = input.nextLine();
        if(userInput.equals("1")){
            return new MeniuProblemedeCalcul(this);
        }else if(userInput.equals("2")){

            return new MeniuStructuriDeDate(this);
        }else if(userInput.equals("3")){
            return new MeniuAlgoritmica(this);
        }else if(userInput.equals("4")){
            return previous;
        }else {
            System.out.println("Optiune invalida! ");
            return this;
        }

    }
}
