package day3.Exercitiu4_Meniu;

import java.util.Scanner;

public class MainManu implements Menu{
    Scanner input = new Scanner(System.in);
    public void draw() {
        System.out.println("1--> Probleme de matematica");
        System.out.println("2--> Probleme de informatica");
        System.out.println("3--> Iesire");
    }

    public Menu select() {
        String userInput = input.nextLine();
        if(userInput.equals("1")){
            return new MeniuMate(this);
        }else if (userInput.equals("2")){
            return new MeniuInfo(this);
        }else if(userInput.equals("3")){
            return null;
        }else {
            System.out.println("Optiune invalida");
            return this;
        }
    }
}
