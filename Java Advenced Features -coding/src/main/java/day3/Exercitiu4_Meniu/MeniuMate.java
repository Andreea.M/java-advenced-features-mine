package day3.Exercitiu4_Meniu;

public class MeniuMate extends BaseMenu{

    public MeniuMate(Menu previous){
        super(previous);
    }

    public void draw() {
        System.out.println("1 -> Logica");
        System.out.println("2 -> Analiza functiilor");
        System.out.println("3 -> Ecuatii diferentiale");
        System.out.println("4 -> Meniu anterior");
    }

    public Menu select() {
        String userInput = input.nextLine();
        if(userInput.equals("1")){
            return new MeniuProblemeLogica(this);
        }else if(userInput.equals("2")){
            return new MeniuAnalizaFunctiilor(this);
        }else if(userInput.equals("3")){
            return new MeniuEcuatiiDiferentiale(this);
        }else if(userInput.equals("4")){
            return previous;
        }else {
            System.out.println("Optiune invalida! ");
            return this;
        }

    }

}
