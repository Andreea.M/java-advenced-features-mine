package day3.TimerTest;

public class Main {
    public static void main(String[] args) {
        int size = 100000;
        Runnable x = new RandomAccesArrayListTest(size);
        MyTimer time1 = new MyTimer(x);
        time1.time();
        Runnable y = new RandomAccesLinkedList(size);
        MyTimer time2 = new MyTimer(y);
        time2.time();
        //System.exit(1);

        Runnable z = new InsertArrayListTest(size);
        //z.run();
        //z.printList();
        MyTimer time3 = new MyTimer(z);
        time3.time();
        Runnable w = new InsertLinkedListTest(size);
        //w.run();
        //w.printList();
        MyTimer time4 = new MyTimer(w);
        time4.time();

    }
}
