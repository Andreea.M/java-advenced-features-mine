package day3.TimerTest;

public class MyTimer {
    private Runnable action;
    public MyTimer(Runnable action) {
        this.action = action;
    }
    public long time() {
        long before = System.nanoTime();
        action.run();//execut secventa de cod;
        long after = System.nanoTime();
        long diff = after - before;
        System.out.println("Testing "  + action + " " + diff + " nanos");
        return diff;
    }
}

