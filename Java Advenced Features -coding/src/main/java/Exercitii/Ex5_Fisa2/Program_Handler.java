package Exercitii.Ex5_Fisa2;

import day1.Exercitiu5_Palindrom;

import java.util.Scanner;

public class Program_Handler {
    private Scanner scanner = new Scanner(System.in);
    public String input;

    public Program_Handler(CallExercices palindorm) {
        super();
    }

    public void run() {
        input = scanner.nextLine();
        System.out.println(new Exercitiu5_Palindrom());
    }

    public String description() {
       return "Introduceti o propozitie ";
    }
}
