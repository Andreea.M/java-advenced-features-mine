package fluenceInterface;

import java.util.ArrayList;
import java.util.List;

public class FoodMenu implements  Menu {
    List<Pizza> pizzaList;
    List<String> selectedPizza;

    public FoodMenu() {
        this.pizzaList = new ArrayList<>();
        this.selectedPizza = new ArrayList<>();
        populateMenu();

    }

    private void populateMenu() {
        Pizza pizza = new PizzaQuatroStagioni();
        pizzaList.add(pizza);
    }

    public Menu orderPizza(List<String> orders) {
        for(String order:orders){
         selectedPizza.add(order);
            System.out.println("I have ordered pizza" + " " + order);
        }
        return this;
    }

    public Menu eatPizza() {
        System.out.println("I have eaten pizza");
        return this;
    }

    public Menu payPizza() {
        System.out.println("I have pay for pizza");
        return this;
    }
}
