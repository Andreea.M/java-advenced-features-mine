package fluenceInterface;

public interface Pizza {
    public Pizza getName();
    public Pizza getIngredientes();
    public Pizza getCost();

}
