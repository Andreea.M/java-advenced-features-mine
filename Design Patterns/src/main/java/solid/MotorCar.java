package solid;

public class MotorCar implements Car{
    private Enginee enginee;
    public MotorCar() {
        this.enginee = new Enginee();
    }

    public void start() {
        System.out.println("i'm a motor car");
    }

    public void accelerate(int kmh) {
        System.out.println("Voltage of motor car");
        enginee.speed(kmh);
    }

    public void stop() {
        System.out.println("Electric brakes");
        enginee.stop();
    }
}
