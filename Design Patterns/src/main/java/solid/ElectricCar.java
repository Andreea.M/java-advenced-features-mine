package solid;

public class ElectricCar implements Car {
private Enginee enginee;

    public ElectricCar() {
        this.enginee = new Enginee();
    }

    public void start() {
        System.out.println("battery car");
        enginee.run();
    }

    public void accelerate(int kph) {
        System.out.println("Battery accelerate");
        enginee.speed(kph);
    }

    public void stop() {
        System.out.println("battery stop");
        enginee.stop();
    }

    public void regeneratingBreak(){
        enginee.stop();
        System.out.println("Regenerating Battery");
    }

}
