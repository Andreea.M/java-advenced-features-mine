package designPatterns.AbstractFactory.factories;

import designPatterns.AbstractFactory.pizza.Pizza;
import designPatterns.AbstractFactory.pizza.PizzaType;

public interface AbstractFactory {
    Pizza create(PizzaType type, int size);
}
