package designPatterns.AbstractFactory.factories;

import designPatterns.AbstractFactory.pizza.*;
import designPatternAbstractFactory.pizza.*;


public class PizzaFactory implements AbstractFactory {
    @Override
    public Pizza create(PizzaType type, int size) {
        switch (type){
            case MARGHERITA:
                return new PizzaMargherita(size);
            case QUATROFORMAGGI:
                return new PizzaQuatroformagi(size);

            case QUATROSTAGIONNI:
                return new PizzaQuatrostagionni(size);

            default: return null;
        }
    }
}
