package designPatterns.AbstractFactory.pizza;

public abstract class Pizza {
    abstract String getName();
    abstract String getIngredients();
    abstract int getSize();

    @Override
    public String toString() {
        return "Pizza comandata: "+ getName() + " " + "contine: " + getIngredients() + ", " + "cu dimensiunea de:" + getSize() + "cm";
    }
}
