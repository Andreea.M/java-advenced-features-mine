package designPatterns.AbstractFactory.pizza;

public enum PizzaType {
    QUATROSTAGIONNI, MARGHERITA, QUATROFORMAGGI
}
