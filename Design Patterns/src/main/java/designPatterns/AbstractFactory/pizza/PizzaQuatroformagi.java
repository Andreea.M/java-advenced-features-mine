package designPatterns.AbstractFactory.pizza;

public class PizzaQuatroformagi extends Pizza{
    private int size;

    public PizzaQuatroformagi(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATROFORMAGGI.toString();
    }

    @Override
    public String getIngredients() {
        return "blat, mozarela, sos rosii, parmezan, brie, gorgonzola";
    }

    @Override
    public int getSize() {
        return size;
    }
}
