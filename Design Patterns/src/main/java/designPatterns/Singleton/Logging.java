package designPatterns.Singleton;

public class Logging {
    private String lastMesage;
    private static Logging instance = null;
    private Logging(){
        lastMesage = new String();
    }

    public static Logging getInstance() {
        if (instance == null) {
            instance = new Logging();
        }
        return instance;
    }

    public void printMessage(String message){
        System.out.println(message);
        this.lastMesage = message;
    }

    @Override
    public String toString() {
        return "Logging{" +
                "lastMesage='" + lastMesage + '\'' +
                '}';
    }
}
