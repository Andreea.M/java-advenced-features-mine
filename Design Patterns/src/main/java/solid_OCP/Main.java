package solid_OCP;

public class Main {
    public static void main(String[] args) {
        IOperation addition = new Addition(5,7);
        IOperation substraction = new Substraction(12, 6);
        Multiplication calc2 = new Multiplication(8,4);
        SimpleCalculator calc1 = new SimpleCalculator();

        calc1.calculate(addition);
        calc1.calculate(substraction);
        calc2.performOperation();




    }
}
