package solid_OCP;

public interface IOperation {
    void performOperation();
}
