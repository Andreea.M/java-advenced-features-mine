package solid_OCP;

public interface ICalculator {
    void calculate(IOperation operation);
}