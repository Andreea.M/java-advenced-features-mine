import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.base.MockitoException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoException.class)
public class PersonServiceMockTest {

    @Mock
    private PersonRepository personRepository;
    @InjectMocks
    private PersonService personService;

    @Test
    public void test() {
        Person person = new Person();
        person.setName("Gabriel");
        person.setPrenume("Ionescu");
        person.setVarsta(18);
        when(personRepository.findById(3)).thenReturn(person);

        Person person3 = personService.increaseAge(3);
        verify(personRepository, atLeastOnce()).findById(3);
        assertEquals(19, person3.getVarsta());
    }
}
