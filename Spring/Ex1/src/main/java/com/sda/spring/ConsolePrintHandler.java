package com.sda.spring;

//@Component
public class ConsolePrintHandler implements IPrintHandler{
    public ConsolePrintHandler() {
        System.out.println(getClass().getSimpleName() + " Created");
    }
    public void print(String message) {
        System.out.println(getClass().getSimpleName() + " message");
    }
}