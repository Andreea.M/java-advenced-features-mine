package com.sda.spring;

import org.springframework.stereotype.Component;

@Component("printHandler")
public class FilePrintHandler implements IPrintHandler{

    public FilePrintHandler(){
        System.out.println(getClass().getSimpleName()+ " created");
    }

    public void print(String message){
        System.out.println(getClass().getSimpleName() + message);
    }
}
