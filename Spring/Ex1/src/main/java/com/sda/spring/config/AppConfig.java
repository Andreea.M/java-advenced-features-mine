package com.sda.spring.config;

import com.sda.spring.handlers.DataBaseConnector;
import com.sda.spring.handlers.ProductHandler;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan("com.sda.spring")
@Import({DataBaseConfig.class, WebConfig.class})

public class AppConfig {
    @Bean
    public ProductHandler createProductHandler() {
        return new ProductHandler();
    }

    @Bean(name = "conn1")
    public DataBaseConnector createConnector1() {
        DataBaseConnector dataBaseConnector = new DataBaseConnector();
        dataBaseConnector.setUrl("Url1");
        return dataBaseConnector;
    }

    @Bean(name = "conn2")
    public DataBaseConnector createConnector2() {
        DataBaseConnector dataBaseConnector = new DataBaseConnector();
        dataBaseConnector.setUrl("Url2");
        return dataBaseConnector;
    }
}
