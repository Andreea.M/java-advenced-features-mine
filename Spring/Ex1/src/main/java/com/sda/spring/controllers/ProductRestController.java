package com.sda.spring.controllers;

import com.sda.spring.entities.ProductEntity;
import com.sda.spring.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductRestController {
    @Autowired
    private ProductRepository productRepository;

    public ProductRestController() {
        System.out.println(getClass().getSimpleName() + " Created");
    }

    @GetMapping("/api/products")
    public List<ProductEntity> getProducts() {
        return productRepository.findAll();
    }

    @PostMapping("/api/products")
    public String saveProduct(@RequestBody ProductEntity productEntity) {
        productRepository.save(productEntity);
        return "Succes";
    }

    //url este de forma: http://localhost:8080/api/products/1
//    @GetMapping("/api/products/{id}")
//    public ProductEntity getProductById(@PathVariable(required = false) Integer id) {
//        return productRepository.findById(id).get();
//    }

    @GetMapping("/api/products/{id}")
    public ResponseEntity <ProductEntity> getProductById(@PathVariable(required = false) Integer id) {
        //return productRepository.findById(id).get();
        return ResponseEntity.status(220).body(productRepository.findById(id).get());
    }
    @DeleteMapping("/api/products/{id}")
    public String deleteProductById(@PathVariable Integer id){
        productRepository.deleteById(id);
        return "delete";
    }


}
