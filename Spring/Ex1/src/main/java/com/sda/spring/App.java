package com.sda.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication

public class App {
    public static void main(String[] args) {
//        AnnotationConfigApplicationContext context =  new AnnotationConfigApplicationContext(App.class);
//
//        UserHandler userHandler = context.getBean("userHandler", UserHandler.class);
//        userHandler.getUser();

        SpringApplication.run(App.class, args);
    }
}

