package com.sda.spring.handlers;

import com.sda.spring.IPrintHandler;
import com.sda.spring.entities.ProductEntity;
import com.sda.spring.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserHandler {
    @Autowired
    private IPrintHandler printHandler;
    @Autowired
    private ProductHandler productHandler;
    @Autowired
    private DataBaseConnector conn1;
    @Autowired
    private DataBaseConnector conn2;
    @Autowired
    private ProductRepository productRepository;

    public UserHandler() {
        System.out.println(getClass().getSimpleName() + " Created");
    }

    @Autowired
    public void getUser() {
        //get user from dataBase
        printHandler.print("Am obtinut user-ul din baza de date");
        List<ProductEntity> productList = this.productRepository.findAll();
        productList.stream().forEach(System.out::println);
        List<ProductEntity> productList1 = this.productRepository.findAllByProductName("Iaurt");
        productList1.stream().forEach(System.out::println);

    }
}