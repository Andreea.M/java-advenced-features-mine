package com.sda.spring.repositories;

import com.sda.spring.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface ProductRepository extends JpaRepository<ProductEntity,Integer> {
    @Query("from ProductEntity where productName=?1")
    List<ProductEntity> getProductByName(String name);

    List<ProductEntity> findAllByProductName(String name);
    List<ProductEntity> findAllByProductNameOrderByProductName(String name);
    List<ProductEntity> findAllByProductNameAndProductId(String name, Integer id);

}
