package com.sda.spring.controllers;

import com.sda.spring.entities.ProductEntity;
import com.sda.spring.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
//    @GetMapping("/web/frontpage")
//    public String getFrontPage(){
//        return "frontpage";
//    }

    @GetMapping("/web/frontpage")
    public ModelAndView getFrontPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("frontpage");
        modelAndView.addObject("header", "page.crud");
        modelAndView.addObject("productList", productRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/web/product/delete/{id}")
    public ModelAndView deleteProduct(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/web/frontpage");
        productRepository.deleteById(id);
        return modelAndView;
    }

    @GetMapping("/web/product/add")
    public ModelAndView addProduct() {
        ModelAndView modelAndView = new ModelAndView("product-form");
        modelAndView.addObject("product", new ProductEntity());
        return modelAndView;
    }

    @PostMapping("/web/product/save")
    public ModelAndView saveProduct(@ModelAttribute("product") ProductEntity productEntity) {
        ModelAndView modelAndView = new ModelAndView("redirect:/web/frontpage");
        productRepository.save(productEntity);
        return modelAndView;
    }
    @GetMapping("/web/product/edit/{id}")
    public ModelAndView editProduct(@PathVariable Integer id){
        ModelAndView modelAndView = new ModelAndView("product-form");
        modelAndView.addObject("product", productRepository.findById(id).get());
        return modelAndView;
    }



}
