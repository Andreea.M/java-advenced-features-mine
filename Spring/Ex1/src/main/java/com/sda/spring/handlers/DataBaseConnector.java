package com.sda.spring.handlers;

public class DataBaseConnector {
    private String url;

    public DataBaseConnector() {
        System.out.println(getClass().getSimpleName() + " Created");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
