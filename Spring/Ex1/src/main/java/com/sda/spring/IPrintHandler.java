package com.sda.spring;

public interface IPrintHandler {
   void print(String message);
}
