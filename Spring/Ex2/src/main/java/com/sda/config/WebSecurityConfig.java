package com.sda.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/", "/login", "/register/save", "/adduser")//url-uri fara autentificare
                .permitAll();
        http.authorizeRequests()
                .antMatchers("/category/edit/**", "/category/delete/**")
                .hasRole("ADMIN");
        http.authorizeRequests()
                .anyRequest()
                .authenticated();//pt toate celelalte trebuie sa ne autentificam
        http.formLogin()
                .loginProcessingUrl("/login")
                .loginPage("/")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/category/list")
                .failureUrl("/");
        http.logout()
                .logoutUrl("/logout")
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
//        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        auth.inMemoryAuthentication()
//                .withUser("Andreea")
//                .password(passwordEncoder.encode("SDA123"))
//                .roles("USER")
//                .and()
//                .withUser("Maria")
//                .password(passwordEncoder.encode("SDA456"))
//                .roles("USER", "ADMIN")
//                .and()
//                .passwordEncoder(passwordEncoder);
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
        System.out.println(passwordEncoder.encode("SDA123"));
    }
}
