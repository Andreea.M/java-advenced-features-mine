package com.sda.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Column(length = 50)
    @Size(min = 3, max = 50, message = "User trebuie sa fie minim 3, maxim 30 caractere!")
    @NotBlank(message = "Userul nu poate fi nul! ")
    private String username;

    @Column(length = 150)
    @Size(min = 3, max = 30, message = "Parola trebuie sa fie minim 3, maxim 30 de caractere!")
    @NotBlank(message = "Parola nu poate fi nula!")
    private String password;

    private Boolean enabled;

    @OneToMany(mappedBy = "user")
    private List<AuthoritiesEntity> authoritiesEntities;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<AuthoritiesEntity> getAuthoritiesEntities() {
        return authoritiesEntities;
    }

    public void setAuthoritiesEntities(List<AuthoritiesEntity> authoritiesEntities) {
        this.authoritiesEntities = authoritiesEntities;
    }
}
