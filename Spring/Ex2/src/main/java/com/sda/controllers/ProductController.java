package com.sda.controllers;

import com.sda.entities.CategoryEntity;
import com.sda.entities.ProductEntity;
import com.sda.repositories.CategoryRepository;
import com.sda.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/")
    public ModelAndView getIndex() {
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }
    @GetMapping("/product/list")
    public ModelAndView getProductsById() {
        ModelAndView modelAndView = new ModelAndView("products");
        modelAndView.addObject("product", productRepository.findAll());
        return modelAndView;
    }
    @GetMapping("/product/add")
    public ModelAndView addProduct() {
        ModelAndView modelAndView = new ModelAndView("product-form");
        modelAndView.addObject("product", new ProductEntity());
        modelAndView.addObject("category", categoryRepository.findAll());
        return modelAndView;
    }
    @PostMapping("/product/save")
    public ModelAndView saveProduct(@ModelAttribute("product") ProductEntity productEntity) {
        ModelAndView modelAndView = new ModelAndView("redirect:/product/list");
        productRepository.save(productEntity);
        return modelAndView;
    }
    @GetMapping("/product/delete/{id}")
    public ModelAndView deleteProduct(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/product/list");
        productRepository.deleteById(id);
        return modelAndView;
    }

    @GetMapping("/product/edit/{id}")
    public ModelAndView editProduct(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("product-form");
        modelAndView.addObject("product", productRepository.findById(id).get());
        return modelAndView;
    }


}
