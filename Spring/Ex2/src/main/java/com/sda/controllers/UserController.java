package com.sda.controllers;

import com.sda.entities.AuthoritiesEntity;
import com.sda.entities.UserEntity;
import com.sda.repositories.AuthorityRepository;
import com.sda.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserController() {
    }

    @GetMapping("/adduser")
    public ModelAndView addUser() {
        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("user", new UserEntity());
        return modelAndView;
    }

    @PostMapping("/register/save")
    public ModelAndView saveUser(@ModelAttribute("user") @Valid UserEntity userEntity, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        if(bindingResult.hasErrors()) {
            modelAndView.setViewName("register");
            modelAndView.addObject("user", userEntity);
            return modelAndView;
        }
        userEntity.setEnabled(true);
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        AuthoritiesEntity authoritiesEntity = new AuthoritiesEntity();
        authoritiesEntity.setUsername(userEntity.getUsername());
        authoritiesEntity.setAuthority("USER");
        authoritiesEntity.setUser(userEntity);
        userRepository.save(userEntity);
        authorityRepository.save(authoritiesEntity);
        return modelAndView;
    }
}
