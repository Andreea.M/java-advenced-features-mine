package com.sda;

import com.sda.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


public class App {
    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }
}
